@file:Suppress("UnstableApiUsage")

import fe.buildsettings.extension.GradlePluginPortalRepository
import fe.buildsettings.extension.MavenRepository
import fe.buildsettings.extension.configureRepositories
import fe.buildsettings.extension.hasJitpackEnv
import fe.buildsettings.extension.includeProject

rootProject.name = "httpkt"

pluginManagement {
    repositories {
        mavenCentral()
        gradlePluginPortal()
        maven { url = uri("https://jitpack.io") }
    }

    extra.properties["gradle.build.dir"]
        ?.let { includeBuild(it.toString()) }

    plugins {
        id("de.fayard.refreshVersions") version "0.60.5"
        id("net.nemerosa.versioning") version "3.1.0"
        kotlin("jvm") version "2.1.0" apply false
    }

    when (val gradleBuildDir = extra.properties["gradle.build.dir"]) {
        null -> {
            val gradleBuildVersion = extra.properties["gradle.build.version"]
            val plugins = mapOf(
                "com.gitlab.grrfe.build-settings-plugin" to "com.gitlab.grrfe.gradle-build:build-settings",
                "com.gitlab.grrfe.build-logic-plugin" to "com.gitlab.grrfe.gradle-build:build-logic"
            )

            resolutionStrategy {
                eachPlugin {
                    plugins[requested.id.id]?.let { useModule("$it:$gradleBuildVersion") }
                }
            }
        }
        else -> includeBuild(gradleBuildDir.toString())
    }
}

plugins {
    id("de.fayard.refreshVersions")
    id("com.gitlab.grrfe.build-settings-plugin")
}

configureRepositories(MavenRepository.MavenCentral, MavenRepository.Jitpack, GradlePluginPortalRepository)

extra.properties["gradle.build.dir"]
    ?.let { includeBuild(it.toString()) }

include(":core")
includeProject(":core2-core", "core2/core")
includeProject(":core2-gson", "core2/gson")
includeProject(":core2-jsoup", "core2/jsoup")

includeProject(":auth-core", "auth/core")
includeProject(":auth-oauth-10a", "auth/oauth-10a")
includeProject(":auth-oauth-20", "auth/oauth-20")

includeProject(":serialization-gson", "serialization/gson")
includeProject(":serialization-jsoup", "serialization/jsoup")

include(":platform")

if (!hasJitpackEnv) {
    include(":demo-testing")
}
