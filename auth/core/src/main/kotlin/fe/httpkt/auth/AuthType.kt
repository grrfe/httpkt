package fe.httpkt.auth

import fe.httpkt.HttpData

public enum class AuthType {
    Basic, Bearer, OAuth
}

public fun AuthType.createHeader(value: String): HttpData = HttpData.of {
    addHeaderImpl("Authorization", "$name $value")
}
