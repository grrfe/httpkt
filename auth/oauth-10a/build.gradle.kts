plugins {

}

dependencies {
    api(project(":core"))
    api(project(":auth-core"))

    implementation("org.apache.httpcomponents.core5:httpcore5:_")
    testImplementation("com.willowtreeapps.assertk:assertk:_")
    testImplementation(kotlin("test"))
}
