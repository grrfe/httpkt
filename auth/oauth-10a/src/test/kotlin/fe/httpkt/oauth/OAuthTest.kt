package fe.httpkt.oauth

import assertk.assertAll
import assertk.assertThat
import assertk.assertions.*
import fe.httpkt.oauth.signing.HmacSHA1OAuthSigner
import fe.httpkt.oauth.signing.OAuthSignedRequest
import fe.httpkt.oauth.signing.OAuthSigner
import fe.httpkt.oauth.signing.OAuthSigningService
import org.apache.hc.core5.http.NameValuePair
import org.apache.hc.core5.net.URIBuilder
import org.apache.hc.core5.net.URLEncodedUtils
import kotlin.test.Test

// Test for https://developer.x.com/en/docs/authentication/oauth-1-0a/creating-a-signature
internal class OAuthTest {
    private fun List<NameValuePair>.toPairList(): List<Pair<String, String>> {
        return map { it.name to it.value }
    }

    private fun unpackHeaderValue(headerValue: String) = buildMap {
        val pairs = headerValue.split(",")
        for (pair in pairs) {
            val (key, value) = pair.trim().split("=")
            this[key] = value.substring(1, value.length - 1)
        }
    }

    private val host = "api.x.com"

    private val expectedParameterString = """include_entities=true&oauth_consumer_key=xvz1evFS4wEEPTGEFPHBog
        |&oauth_nonce=kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg&oauth_signature_method=HMAC-SHA1
        |&oauth_timestamp=1318622958&oauth_token=370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb
        |&oauth_version=1.0&status=Hello%20Ladies%20%2B%20Gentlemen%2C%20a%20signed%20OAuth%20request%21""".trimMargin().replace("\n", "")

    private  val expectedSignatureBaseString = """POST&https%3A%2F%2Fapi.x.com%2F1.1%2Fstatuses%2Fupdate.json
        |&include_entities%3Dtrue%26oauth_consumer_key%3Dxvz1evFS4wEEPTGEFPHBog%26oauth_nonce%3DkYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg
        |%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D1318622958%26oauth_token%3D370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb
        |%26oauth_version%3D1.0%26status%3DHello%2520Ladies%2520%252B%2520Gentlemen%252C%2520a%2520signed%2520OAuth%2520request%2521""".trimMargin().replace("\n", "")

    private  val expectedHeaderValueParts = mapOf(
        "realm" to "http://$host/",
        "oauth_consumer_key" to "xvz1evFS4wEEPTGEFPHBog",
        "oauth_nonce" to "kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg",
        "oauth_signature" to "Ls93hJiZbQ3akF3HF3x1Bz8%2FzU4%3D",
        "oauth_signature_method" to "HMAC-SHA1",
        "oauth_timestamp" to "1318622958",
        "oauth_token" to "370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb",
        "oauth_version" to "1.0"
    )

    @Test
    fun test() {
        val method = "POST"
        val url = "https://$host/1.1/statuses/update.json?include_entities=true"

        val builder = URIBuilder(url)

        val body = "status=Hello%20Ladies%20%2b%20Gentlemen%2c%20a%20signed%20OAuth%20request%21"
        val query = builder.queryParams.toPairList()
        val bodyParams = URLEncodedUtils.parse(body, Charsets.UTF_8).toPairList()

        val urlWithoutQuery = builder.removeQuery().toString()

        val request = OAuthSignedRequest(
            verb = method,
            urlWithoutQuery = urlWithoutQuery,
            queryParameters = query + bodyParams,
            nonce = "kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg",
            timestamp = "1318622958"
        )

        val signer: OAuthSigner = HmacSHA1OAuthSigner
        val oauthContext = OAuthContext(
            realm = "http://$host/",
            version = "1.0",
            token = "370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb",
            consumerKey = "xvz1evFS4wEEPTGEFPHBog",
            signatureMethod = signer.method
        )

        val service = OAuthSigningService(
            signer,
            oauthContext,
            "kAcSOqF21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3Z7kBw",
            "LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE"
        )

        assertAll {
            val parameterString = service.buildParameterString(request)
            assertThat(parameterString).isEqualTo(expectedParameterString)

            val signatureBaseString = service.makeSignatureBaseString(request, parameterString)
            assertThat(signatureBaseString).isEqualTo(expectedSignatureBaseString)

            val signature = service.createSignature(request, parameterString, signatureBaseString)
            assertThat(signature).isEqualTo("Ls93hJiZbQ3akF3HF3x1Bz8/zU4=")

            val headerValue = oauthContext.formatHeader(request, signature)
            val headerValueParts = unpackHeaderValue(headerValue)

            assertThat(headerValueParts).isEqualTo(expectedHeaderValueParts)
        }
    }
}
