package fe.httpkt.oauth

public interface OAuthNonceGenerator {
    public fun generate(): String
}
