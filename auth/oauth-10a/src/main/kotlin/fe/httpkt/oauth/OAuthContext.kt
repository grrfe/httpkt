package fe.httpkt.oauth

import fe.httpkt.oauth.signing.OAuthSignedRequest

public data class OAuthContext(
    val realm: String,
    val version: String = "1.0",
    val token: String,
    val consumerKey: String,
    val signatureMethod: String,
) {
    private val baseOAuthConstants = buildList {
        add(OAuthConstants.VERSION to version)
        add(OAuthConstants.TOKEN to token)
        add(OAuthConstants.CONSUMER_KEY to consumerKey)
        add(OAuthConstants.SIGNATURE_METHOD to signatureMethod)
    }

    private fun toAuthorizationValue(realm: String, request: OAuthSignedRequest, signature: String) = baseOAuthConstants + buildList {
        add(OAuthConstants.REALM to realm)
        add(OAuthConstants.SIGNATURE to signature)
        addAll(request.oauthRequestIdentifiers)
    }

    private fun Pair<String, String>.toAuthorizationPair(): String {
        return "$first=\"$second\""
    }

    public fun formatHeader(request: OAuthSignedRequest, signature: String): String {
        return toAuthorizationValue(
            realm,
            request,
            OAuthEncoder.encode(signature)
        ).joinToString(separator = ", ") { it.toAuthorizationPair() }
    }

    public fun collectParameters(request: OAuthSignedRequest): List<Pair<String, String>> {
        return request.queryParameters + request.oauthRequestIdentifiers + baseOAuthConstants
    }
}
