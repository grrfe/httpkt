package fe.httpkt.oauth.signing

import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

public sealed class OAuthSigner(public val method: String, public val algorithm: String) {
    public abstract fun sign(key: String, payload: String): ByteArray
}

public data object HmacSHA1OAuthSigner : OAuthSigner("HMAC-SHA1", "HmacSHA1") {
    override fun sign(key: String, payload: String): ByteArray {
        val secretKeySpec = SecretKeySpec(key.toByteArray(), algorithm)
        val mac = Mac.getInstance(algorithm).apply { init(secretKeySpec) }

        return mac.doFinal(payload.toByteArray())
    }
}
