package fe.httpkt.oauth

public object OAuthConstants {
    public const val REALM: String = "realm"
    public const val VERSION: String = "oauth_version"
    public const val TOKEN: String = "oauth_token"
    public const val NONCE: String = "oauth_nonce"
    public const val TIMESTAMP: String = "oauth_timestamp"
    public const val CONSUMER_KEY: String = "oauth_consumer_key"
    public const val SIGNATURE: String = "oauth_signature"
    public const val SIGNATURE_METHOD: String = "oauth_signature_method"
}
