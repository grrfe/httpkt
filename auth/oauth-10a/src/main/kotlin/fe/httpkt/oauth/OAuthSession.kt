package fe.httpkt.oauth

import fe.httpkt.HttpData
import fe.httpkt.HttpDataBuilder
import fe.httpkt.Request
import fe.httpkt.auth.AuthType
import fe.httpkt.auth.createHeader
import fe.httpkt.body.BodyData
import fe.httpkt.oauth.signing.OAuthSignedRequest
import fe.httpkt.oauth.signing.OAuthSigner
import fe.httpkt.oauth.signing.OAuthSigningService
import java.net.HttpURLConnection

public open class OAuthSession(
    private val oauthContext: OAuthContext,
    signer: OAuthSigner,
    private val nonceGenerator: OAuthNonceGenerator,
    apiSecret: String,
    oauthSecret: String,
    data: HttpDataBuilder
) : Request(data = data) {

    private val service = OAuthSigningService(
        signer, oauthContext, apiSecret, oauthSecret
    )

    private fun mergeData(data: HttpData?): HttpData {
        return if (data != null) this.instanceData + data else this.instanceData
    }

    private fun createSignature(request: OAuthSignedRequest): HttpData {
        val signature = service.createSignature(request)
        val headerValue = oauthContext.formatHeader(request, signature)

        return AuthType.OAuth.createHeader(headerValue)
    }

    override fun get(
        url: String,
        getParams: Map<Any, Any>,
        data: HttpData?,
        connectTimeout: Int,
        readTimeout: Int,
        followRedirects: Boolean,
        maxRedirects: Int,
        forceSend: Boolean
    ): HttpURLConnection {
        val parameters = getParams.map { (key, value) -> key.toString() to value.toString() }
        val request = OAuthSignedRequest.create("GET", url, parameters, nonceGenerator)
        val authHeader = createSignature(request)

        return super.get(
            url,
            getParams,
            mergeData(data) + authHeader,
            connectTimeout,
            readTimeout,
            followRedirects,
            maxRedirects,
            forceSend
        )
    }

    override fun request(
        method: HttpMethod,
        url: String,
        body: BodyData,
        data: HttpData?,
        connectTimeout: Int,
        readTimeout: Int,
        followRedirects: Boolean,
        maxRedirects: Int,
        forceSend: Boolean
    ): HttpURLConnection {
        val request = OAuthSignedRequest.create(method.name.uppercase(), url, emptyList(), nonceGenerator)
        val authHeader = createSignature(request)

        return super.request(
            method,
            url,
            body,
            mergeData(data) + authHeader,
            connectTimeout,
            readTimeout,
            followRedirects,
            maxRedirects,
            forceSend
        )
    }
}
