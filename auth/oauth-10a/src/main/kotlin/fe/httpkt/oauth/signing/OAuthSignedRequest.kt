package fe.httpkt.oauth.signing

import fe.httpkt.oauth.OAuthConstants
import fe.httpkt.oauth.OAuthNonceGenerator

public data class OAuthSignedRequest(
    val verb: String,
    val urlWithoutQuery: String,
    val queryParameters: List<Pair<String, String>>,
    val nonce: String,
    val timestamp: String = (System.currentTimeMillis() / 1000).toString(),
) {
    val oauthRequestIdentifiers: List<Pair<String, String>> = buildList {
        add(OAuthConstants.NONCE to nonce)
        add(OAuthConstants.TIMESTAMP to timestamp)
    }

    public companion object {
        public fun create(
            verb: String,
            url: String,
            queryParameters: List<Pair<String, String>>,
            nonceGenerator: OAuthNonceGenerator,
        ): OAuthSignedRequest {
            val nonce = nonceGenerator.generate()
            return OAuthSignedRequest(verb, url, queryParameters, nonce)
        }
    }
}
