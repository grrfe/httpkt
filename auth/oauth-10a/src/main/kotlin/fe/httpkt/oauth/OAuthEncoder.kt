package fe.httpkt.oauth

import java.net.URLDecoder
import java.net.URLEncoder
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.util.regex.Pattern

public object OAuthEncoder {
    private val CHARSET: Charset = StandardCharsets.UTF_8
    private val ENCODING_RULES = arrayOf(
        "*".toQuotedRegex() to "%2A",
        "+".toQuotedRegex() to "%20",
        "~".toQuotedRegex() to "%7E"
    )

    private fun String.toQuotedRegex(): Regex {
        return Pattern.quote(this).toRegex()
    }

    public fun encode(plain: String): String {
        var encoded: String = URLEncoder.encode(plain, CHARSET)

        for ((regex, replacement) in ENCODING_RULES) {
            encoded = applyRule(encoded, regex, replacement)
        }

        return encoded
    }

    private fun applyRule(encoded: String, regex: Regex, replacement: String): String {
        return encoded.replace(regex, replacement)
    }

    public fun decode(encoded: String?): String {
        return URLDecoder.decode(encoded, CHARSET)
    }
}
