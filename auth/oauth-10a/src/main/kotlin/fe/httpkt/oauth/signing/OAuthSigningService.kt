package fe.httpkt.oauth.signing

import fe.httpkt.oauth.OAuthContext
import fe.httpkt.oauth.OAuthEncoder
import kotlin.io.encoding.Base64
import kotlin.io.encoding.ExperimentalEncodingApi

public class OAuthSigningService(
    private val signer: OAuthSigner,
    private val context: OAuthContext,
    private val consumerSecret: String,
    private val oauthSecret: String,
) {
    @OptIn(ExperimentalEncodingApi::class)
    public fun createSignature(
        request: OAuthSignedRequest,
        parameterString: String = buildParameterString(request),
        signatureBaseString: String = makeSignatureBaseString(request, parameterString),
    ): String {
        val key = OAuthEncoder.encode(consumerSecret) + "&" + OAuthEncoder.encode(oauthSecret)
        val signature = signer.sign(key, signatureBaseString)

        return Base64.Default.encode(signature)
    }

    public fun buildParameterString(request: OAuthSignedRequest): String {
        return context.collectParameters(request)
            .map { (name, value) -> OAuthEncoder.encode(name) to OAuthEncoder.encode(value) }
            .sortedBy { it.first }
            .joinToString(separator = "&") { (name, value) -> "${name}=${value}" }
    }

    public fun makeSignatureBaseString(request: OAuthSignedRequest, parameterString: String): String {
        return makeSignatureBaseString(request.verb, request.urlWithoutQuery, parameterString)
    }

    private fun makeSignatureBaseString(verb: String, url: String, parameterString: String): String {
        return arrayOf(verb, url, parameterString).joinToString(separator = "&") { OAuthEncoder.encode(it) }
    }
}
