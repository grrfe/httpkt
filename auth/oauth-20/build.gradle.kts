plugins {

}

dependencies {
    api(project(":core"))
    api(project(":core2-core"))
    api(project(":core2-gson"))

    api(project(":serialization-gson"))
    api(project(":auth-core"))

    implementation(KotlinX.coroutines.core)
    implementation("org.apache.httpcomponents.core5:httpcore5:_")
    testImplementation("com.willowtreeapps.assertk:assertk:_")
    testImplementation(kotlin("test"))
}

