package fe.httpkt.oauth20.device

import fe.httpkt.oauth20.error.OAuthErrorCompanion
import fe.httpkt.oauth20.error.OAuthError
import fe.httpkt.oauth20.error.OAuthErrorTypeAdapter

public sealed interface OAuthDeviceCodeError : OAuthError {
    public data object AuthorizationPending : OAuthDeviceCodeError
    public data object SlowDown : OAuthDeviceCodeError
    public data object AccessDenied : OAuthDeviceCodeError
    public data object ExpiredToken : OAuthDeviceCodeError

    public companion object : OAuthErrorCompanion {
        public override fun fromString(value: String): OAuthError {
            return when (value) {
                "authorization_pending" -> AuthorizationPending
                "slow_down" -> SlowDown
                "access_denied" -> AccessDenied
                "expired_token" -> ExpiredToken
                else -> OAuthError.fromString(value)
            }
        }
    }
}

public class OAuthDeviceCodeErrorTypeAdapter : OAuthErrorTypeAdapter(error = OAuthDeviceCodeError)

