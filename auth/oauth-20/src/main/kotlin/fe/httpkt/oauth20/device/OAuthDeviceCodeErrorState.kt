package fe.httpkt.oauth20.device

import com.google.gson.annotations.JsonAdapter
import fe.httpkt.oauth20.error.OAuthErrorState

public class OAuthDeviceCodeErrorState(
    @JsonAdapter(OAuthDeviceCodeErrorTypeAdapter::class)
    public val error: OAuthDeviceCodeError,
) : OAuthErrorState()
