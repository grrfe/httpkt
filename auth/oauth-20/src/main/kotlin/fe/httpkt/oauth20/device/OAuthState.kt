package fe.httpkt.oauth20.device

import com.google.gson.annotations.SerializedName
import fe.httpkt.oauth20.AbsoluteExpiry

public interface OAuthState

public data object DeviceCodeRequestState : OAuthState

public data class DeviceCodeResponseState(
    @SerializedName("device_code") val deviceCode: String,
    @SerializedName("user_code") val userCode: String,
    @SerializedName("verification_uri", alternate = ["verification_url"]) val verificationUri: String,
    @SerializedName("verification_uri_complete") val verificationUriComplete: String? = null,
    @SerializedName("expires_in") override val expiresIn: Long,
    @SerializedName("interval") val interval: Long = 5L,
) : OAuthState, AbsoluteExpiry {
    override var expiresAt: Long? = null
}

public data class AccessTokenResponseState(
    @SerializedName("access_token") val accessToken: String,
    @SerializedName("refresh_token") val refreshToken: String? = null,
    @SerializedName("expires_in") override val expiresIn: Long,
    @SerializedName("type", alternate = ["token_type"]) val type: String? = null,
) : OAuthState, AbsoluteExpiry {
    override var expiresAt: Long? = null
}

public data object UserCodeExpiredState : OAuthState
