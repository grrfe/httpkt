package fe.httpkt.oauth20.device

import fe.httpkt.HttpData
import fe.httpkt.body.FormUrlEncodedBody
import fe.httpkt.internal.HttpInternals
import fe.httpkt.internal.RealHttpInternals
import fe.httpkt.oauth20.error.ErrorState
import fe.httpkt.oauth20.error.RequestFailure
import fe.httpkt.oauth20.isExpired
import fe.httpkt.oauth20.trySetupAbsoluteExpiry
import fe.httpkt2.*
import fe.httpkt2.gson.fromJsonToEither
import kotlinx.coroutines.delay

public class HttpKtOAuthDeviceCodeRequest(
    private val internals: HttpInternals = RealHttpInternals(),
    instanceData: HttpData = HttpData.default,
    engine: HttpEngine = HttpEngine(internals),
    private val now: () -> Long = { System.currentTimeMillis() },
    private val retryOnTimeout: () -> Boolean = { false },
    private val tryRecover: ((ErrorState) -> OAuthState)? = null,
    private val onNextState: ((OAuthState) -> Unit)? = null,
    private val clientId: String,
    scopes: List<String>,
    private val codeUrl: String,
    private val tokenUrl: String,
    private val refreshUrl: String,
) : HttpKtRequest(internals, instanceData, engine) {

    private var accessTokens: AccessTokenResponseState? = null

    private val scope = scopes.joinToString(separator = " ")

    private suspend fun prepareState(): OAuthStateResult {
        val state = accessTokens ?: DeviceCodeRequestState
        return handle(state)
    }

    private fun AccessTokenResponseState.mergeAuthData(data: HttpData?): HttpData {
        val auth = HttpData.of { addHeader("Authorization", "Bearer $accessToken") }
        return data?.plus(auth) ?: auth
    }

    public override suspend fun get(
        url: Url,
        data: HttpData?,
        timeouts: Timeouts,
        redirects: Redirects,
        forceSend: Boolean,
    ): HttpResult2 {
        val result = prepareState()
        val state = result.state as? AccessTokenResponseState

        if (state != null) {
            return super.get(url, state.mergeAuthData(data), timeouts, redirects, forceSend)
        }

        error("Yeet")
    }

    private suspend fun refreshAccessToken(refreshToken: String): OAuthState {
        val body = FormUrlEncodedBody(
            "client_id" to clientId,
            "grant_type" to "refresh_token",
            "refresh_token" to refreshToken
        )

        return post(url = Url(refreshUrl), body = body)
            .asEither()
            .foldHttpSuccess(
                onSuccess = { it.fromJsonToEither<AccessTokenResponseState>() },
                onFailure = { it.fromJsonToEither<OAuthDeviceCodeErrorState>() }
            )
            .conflate(
                transformLeft = { RequestFailure(it) },
                transformRight = { it.trySetupAbsoluteExpiry(now) }
            )
    }

    private suspend fun requestCode(): OAuthState {
        val body = FormUrlEncodedBody("client_id" to clientId, "scope" to scope)
        return post(url = Url(codeUrl), body = body)
            .asEither()
            .foldHttpSuccess(
                onSuccess = { it.fromJsonToEither<DeviceCodeResponseState>() },
                onFailure = { it.fromJsonToEither<OAuthDeviceCodeErrorState>() }
            )
            .conflate(
                transformLeft = { RequestFailure(it) },
                transformRight = { it.trySetupAbsoluteExpiry(now) }
            )
    }

    private suspend fun pollToken(deviceCode: String): AccessTokenResponseState? {
        val body = FormUrlEncodedBody(
            "client_id" to clientId,
            // TODO: Debrid link uses just "code"
            "code" to deviceCode,
            "device_code" to deviceCode,
            "grant_type" to "urn:ietf:params:oauth:grant-type:device_code",
        )

        return post(url = Url(tokenUrl), body = body)
            .asEither()
            .mapHttpSuccess { it.fromJsonToEither<AccessTokenResponseState>() }
            ?.also { it.trySetupAbsoluteExpiry(now) }
    }

    private suspend fun handle(state: OAuthState): OAuthStateResult {
        onNextState?.invoke(state)

        val nextState = tryProduceNext(state)
        return when (nextState) {
            null -> OAuthStateResult(state)
            else -> handle(nextState)
        }
    }

    private suspend fun tryProduceNext(state: OAuthState): OAuthState? {
        return when (state) {
            is DeviceCodeRequestState -> requestCode()
            is DeviceCodeResponseState if state.isExpired(now) -> UserCodeExpiredState
            is DeviceCodeResponseState -> pollToken(deviceCode = state.deviceCode) ?: run {
                delay(state.interval * 1000L)
                state
            }

            UserCodeExpiredState if retryOnTimeout() -> DeviceCodeRequestState
            is ErrorState if tryRecover != null -> tryRecover(state)
            is AccessTokenResponseState if state.refreshToken != null && state.isExpired(now) -> refreshAccessToken(
                state.refreshToken
            )

            is AccessTokenResponseState -> {
                accessTokens = state
                // Still return null here to indicate we are finished, we just need to setup our internal state here first
                null
            }

            else -> null
        }
    }

    public fun restore(accessToken: String, refreshToken: String?, expiresAt: Long?) {
        val expiresIn = expiresAt?.let { it - now() } ?: 0
        val state = AccessTokenResponseState(accessToken, refreshToken, expiresIn, "bearer").apply {
            this.expiresAt = expiresAt
        }

        this.accessTokens = state
    }
}

public data class OAuthStateResult(val state: OAuthState)
