package fe.httpkt.oauth20

import fe.httpkt.oauth20.device.OAuthState

public interface AbsoluteExpiry {
    public val expiresIn: Long
    public var expiresAt: Long?
}

public fun AbsoluteExpiry.isExpired(now: () -> Long): Boolean {
    return expiresAt?.let { now() > it } != false
}

public fun OAuthState.trySetupAbsoluteExpiry(now: () -> Long): OAuthState {
    if (this is AbsoluteExpiry) {
        expiresAt = now() + expiresIn * 1000
    }

    return this
}
