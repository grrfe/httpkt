package fe.httpkt.oauth20.error

import com.google.gson.annotations.SerializedName
import fe.httpkt.oauth20.device.OAuthState

public interface ErrorState : OAuthState

public data class RequestFailure(val throwable: Throwable?) : ErrorState

public open class OAuthErrorState(
    @SerializedName("error_description")
    public val description: String? = null,
    @SerializedName("error_uri")
    public val uri: String? = null,
) : ErrorState
