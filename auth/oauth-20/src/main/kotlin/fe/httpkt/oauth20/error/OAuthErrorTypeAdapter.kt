package fe.httpkt.oauth20.error

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter

public abstract class OAuthErrorTypeAdapter(private val error: OAuthErrorCompanion) : TypeAdapter<OAuthError>() {
    override fun write(out: JsonWriter, value: OAuthError) {
        error("Serialization is not supported!")
    }

    override fun read(reader: JsonReader): OAuthError {
        return error.fromString(reader.nextString())
    }
}
