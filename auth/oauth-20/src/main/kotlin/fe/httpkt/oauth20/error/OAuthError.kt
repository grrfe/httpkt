package fe.httpkt.oauth20.error

public interface OAuthError {
    public data object InvalidRequest : OAuthError
    public data object InvalidClient : OAuthError
    public data object InvalidGrant : OAuthError
    public data object UnauthorizedClient : OAuthError
    public data object UnsupportedGrantType : OAuthError
    public data object InvalidScope : OAuthError

    // Non-spec
    public data object UnknownError : OAuthError

    public companion object : OAuthErrorCompanion {
        public override fun fromString(value: String): OAuthError {
            return when (value) {
                "invalid_request" -> InvalidRequest
                "invalid_client" -> InvalidClient
                "invalid_grant" -> InvalidGrant
                "unauthorized_client" -> UnauthorizedClient
                "unsupported_grant_type" -> UnsupportedGrantType
                "invalid_scope" -> InvalidScope
                else -> UnknownError
            }
        }
    }
}

public interface OAuthErrorCompanion {
    public fun fromString(value: String): OAuthError
}

