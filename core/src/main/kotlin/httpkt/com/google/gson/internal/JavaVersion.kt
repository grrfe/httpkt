/*
 * Copyright (C) 2017 The Gson authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package httpkt.com.google.gson.internal

/** Utility to check the major Java version of the current JVM.  */
public object JavaVersion {
    /**
     * Gets the major Java version
     *
     * @return the major Java version, i.e. '8' for Java 1.8, '9' for Java 9 etc.
     */
    // Oracle defines naming conventions at
    // http://www.oracle.com/technetwork/java/javase/versioning-naming-139433.html
    // However, many alternate implementations differ. For example, Debian used 9-debian as the
    // version string
    public val majorJavaVersion: Int by lazy {
        determineMajorJavaVersion()
    }

    private fun determineMajorJavaVersion(): Int {
        val javaVersion = System.getProperty("java.version")
        return parseMajorJavaVersion(javaVersion)
    }

    // Visible for testing only
    public fun parseMajorJavaVersion(javaVersion: String): Int {
        var version = parseDotted(javaVersion)
        if (version == -1) {
            version = extractBeginningInt(javaVersion)
        }
        if (version == -1) {
            return 8 // Choose minimum supported JDK version as default
        }
        return version
    }

    // Parses both legacy 1.8 style and newer 9.0.4 style
    private fun parseDotted(javaVersion: String): Int {
        return try {
            val parts = javaVersion.split("[._]".toRegex(), limit = 3).toTypedArray()
            val firstVer = parts[0].toInt()

            if (firstVer == 1 && parts.size > 1) parts[1].toInt() else firstVer
        } catch (e: NumberFormatException) {
            -1
        }
    }

    private fun extractBeginningInt(javaVersion: String): Int {
        return try {
            val num = StringBuilder()
            for (element in javaVersion) {
                if (Character.isDigit(element)) {
                    num.append(element)
                } else {
                    break
                }
            }

            num.toString().toInt()
        } catch (e: NumberFormatException) {
            -1
        }
    }

    public val isJava9OrLater: Boolean by lazy { majorJavaVersion >= 9 }
}
