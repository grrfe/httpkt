package fe.httpkt.data

import java.net.URLEncoder

public fun buildGetString(vararg pairs: Pair<Any, Any>): String = buildGetString(pairs.toMap())
public fun buildGetString(map: Map<Any, Any>): String = buildString {
    var index = 0
    map.forEach { (key, value) ->
        this.append(if (index++ == 0) "?" else "&")
            .append(key.urlEncode())
            .append("=")
            .append(value.urlEncode())
    }
}

public fun Any.urlEncode() = URLEncoder.encode(this.toString(), "utf-8")
