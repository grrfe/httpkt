package fe.httpkt.data

public object HttpHeaders {
    public const val ContentType: String = "Content-Type"
    public const val ContentEncoding: String = "Content-Encoding"
}

@Deprecated(message = "Use new API", replaceWith = ReplaceWith("HttpHeaders.ContentEncoding"))
public const val HEADER_CONTENT_ENCODING: String = "Content-Encoding"
public const val HEADER_CONTENT_LENGTH: String = "Content-Length"

@Deprecated(message = "Use new API", replaceWith = ReplaceWith("HttpHeaders.ContentType"))
public const val HEADER_CONTENT_TYPE: String = "Content-Type"
public const val HEADER_CONTENT_DISPOSITION: String = "Content-Disposition"

public open class Header(public val name: String, public val values: MutableList<String>) {

    public constructor(name: String, vararg values: String) : this(name, values.toMutableList())

    public fun toValueString(): String = values.joinToString(",")

    public fun firstOrNull(): String? = values.firstOrNull()

    public fun contains(value: String): Boolean = value in this.values

    override fun toString(): String {
        return buildString {
            this.append(name).append(": ").append(toValueString())
        }
    }

    override fun equals(other: Any?): Boolean {
        return if (other is Header) values == other.values else false
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + values.hashCode()
        return result
    }
}

@DslMarker
internal annotation class HeaderDsl

@HeaderDsl
@JvmInline
public value class HeaderDslNode(
    public val headers: MutableMap<String, MutableList<String>> = mutableMapOf<String, MutableList<String>>(),
) {
    public operator fun String.plusAssign(value: String) {
        headers[this] = mutableListOf(value)
    }

    public operator fun String.plusAssign(values: Collection<String>) {
        headers[this] = values.toMutableList()
    }

    public operator fun String.invoke(vararg values: String) {
        headers[this] = mutableListOf(*values)
    }

    public fun build(): List<Header> = headers.map { Header(it.key, it.value) }
}

public data class HeaderDslBuilder(val name: String) {
    val values: MutableList<String> = mutableListOf<String>()
}
