package fe.httpkt.data

import fe.httpkt.util.Extension

public data class Domain private constructor(
    val fullDomain: String, val mainDomain: String, val subDomains: List<String>
) {
    public fun isCookieDomain(url: String): Boolean {
        val (urlMainDomain, urlSubDomains) = Extension.splitDomain(Extension.urlToDomain(url))
        if (mainDomain == urlMainDomain) {
            if ((subDomains.isEmpty() && urlSubDomains.isNotEmpty()) || (subDomains.isEmpty() && urlSubDomains.isEmpty())) {
                return true
            }

            if (subDomains.size <= urlSubDomains.size) {
                for (i in 0 until subDomains.size.coerceAtLeast(urlSubDomains.size)) {
                    val sub1 = subDomains.getOrNull(i)
                    val sub2 = urlSubDomains[i]

                    if (sub1 == null) return true
                    if (sub1 != sub2) return false
                }

                return true
            }
        }

        return false
    }

    public companion object {
        public fun parse(urlOrDomain: String): Domain {
            val fullDomain = Extension.urlToDomain(urlOrDomain)
            val (mainDomain, subDomains) = Extension.splitDomain(fullDomain)

            return Domain(fullDomain, mainDomain, subDomains)
        }
    }
}

public fun String.toDomain() = Domain.parse(this)
