package fe.httpkt.data

import fe.httpkt.util.Extension
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

public data class Cookie private constructor(
    val name: String,
    var value: String,
    var expires: LocalDateTime? = null,
    var maxAge: Int = -1,
    var domain: Domain?,
    var path: String = "/",
    val createdAt: LocalDateTime = LocalDateTime.now()
) {

    public class Builder(
        public var name: String,
        public var value: String = "",
        public var expires: LocalDateTime? = null,
        public var maxAge: Int = -1,
        public var domain: Domain? = null,
        public var path: String = "/"
    ) {
        public fun build(): Cookie = Cookie(name, value, expires, maxAge, domain, path)
    }

    public fun hasExpired(now: LocalDateTime = LocalDateTime.now()): Boolean {
        if (this.maxAge != -1) {
            return createdAt.plusSeconds(maxAge.toLong()).isBefore(now)
        }

        expires?.let {
            return it.isBefore(now)
        }

        return false
    }

    public fun matches(url: String): Boolean {
        return this.domain == null || this.domain!!.isCookieDomain(url)
    }

    public fun toKeyValue(): String = "$name=$value"

    override fun equals(other: Any?): Boolean {
        return (other as? Cookie)?.name == name
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

    public companion object {
        private val DTF_DASHED = DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm:ss")
        private val DTF_DASHED_2 = DateTimeFormatter.ofPattern("dd-MMM-yy HH:mm:ss")
        private val DTF_SPEC = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm:ss")

        private val FORMATTER = listOf(DTF_SPEC, DTF_DASHED, DTF_DASHED_2)

        private fun parseCookieDateTimeString(str: String): LocalDateTime? {
            FORMATTER.forEach { formatter ->
                try {
                    return LocalDateTime.parse(str, formatter)
                } catch (_: DateTimeParseException) {
                }
            }

            return null
        }

        public fun builder(key: String, value: String): Builder = Builder(name = key, value = value)

        public fun of(key: String, value: String): Cookie = builder(key, value).build()

        public fun of(key: String, value: String, builder: Builder.() -> Unit): Cookie = builder(key, value).apply(builder).build()

        private const val keyValSeparator = "="
        public const val keyValDelimiter: String = ";"

        public fun create(cookieHeader: String, url: String? = null): Cookie {
            //Set-Cookie: <cookie-name>=<cookie-value>; Expires=<date>
            val cookie =
                if (cookieHeader.startsWith(Extension.setCookieHeader) || cookieHeader.startsWith(Extension.setCookieHeader.lowercase())) {
                    cookieHeader.substring(Extension.setCookieHeader.length + 2)
                } else cookieHeader

            val firstDelimiterIdx = with(cookie.indexOf(keyValDelimiter)) {
                if (this == -1) cookie.length else this
            }

            val cookieKeyVal = cookie.substring(0, firstDelimiterIdx)
            val keyValIdx = cookieKeyVal.indexOf(keyValSeparator)
            val cookieValue = cookieKeyVal.substring(keyValIdx + 1, cookieKeyVal.length).takeIf { it.isNotEmpty() } ?: ""

            val builder = Builder(
                name = cookieKeyVal.substring(0, keyValIdx).trim(),
                value = cookieValue,
                domain = url?.toDomain()
            )

            if (firstDelimiterIdx + 1 < cookie.length) {
                cookie.substring(firstDelimiterIdx + 1).split(keyValDelimiter).forEach {
                    if (it.contains(keyValSeparator)) {
                        val (key, value) = it.split(keyValSeparator)
                        when (key.trim().lowercase()) {
                            "expires" -> {
                                val dateTime = value.substring(value.indexOf(",") + 2, value.indexOf("GMT") - 1)
                                builder.expires = parseCookieDateTimeString(dateTime)
                            }

                            "max-age" -> {
                                value.toIntOrNull()?.let { i ->
                                    builder.maxAge = i
                                }
                            }

                            "domain" -> {
                                builder.domain = value.toDomain()
                            }

                            "path" -> {
                                builder.path = value
                            }
                        }
                    }
                }
            }

            return builder.build()
        }
    }
}
