package fe.httpkt

public val debuggingEnabled: Boolean = System.getenv("httpkt-debug")?.toBooleanStrictOrNull() == true
