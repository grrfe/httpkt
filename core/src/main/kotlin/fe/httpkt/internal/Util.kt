package fe.httpkt.internal

import java.net.HttpURLConnection

public fun HttpURLConnection.forceSend(forceSend: Boolean): HttpURLConnection {
    // Java seems to only open the socket to the target host if getInputStream is called
    if (forceSend) {
        inputStream
    }

    return this
}
