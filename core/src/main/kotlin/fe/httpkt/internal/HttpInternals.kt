package fe.httpkt.internal

import fe.httpkt.HttpData
import java.net.HttpURLConnection

public interface HttpInternals {
    public fun setupConnection(
        requestMethod: String,
        url: String,
        getParams: Map<Any, Any>,
        newData: HttpData,
        connectTimeout: Int,
        readTimeout: Int,
    ): HttpURLConnection

    public fun applyRequestProperties(con: HttpURLConnection, data: HttpData): HttpURLConnection
    public fun handleRedirections(
        con: HttpURLConnection,
        followRedirects: Boolean,
        maxRedirects: Int,
        data: HttpData,
    ): HttpURLConnection

    public fun followRedirect(con: HttpURLConnection, data: HttpData): HttpURLConnection?
}

