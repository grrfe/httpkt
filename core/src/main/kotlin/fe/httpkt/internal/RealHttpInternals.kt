package fe.httpkt.internal

import fe.httpkt.HttpData
import fe.httpkt.data.Cookie
import fe.httpkt.data.buildGetString
import fe.httpkt.debuggingEnabled
import fe.httpkt.ext.getUrlLocation
import fe.httpkt.ext.isHttpRedirect
import java.net.HttpURLConnection

public class RealHttpInternals(
) : HttpInternals {

    override fun setupConnection(
        requestMethod: String,
        url: String,
        getParams: Map<Any, Any>,
        newData: HttpData,
        connectTimeout: Int,
        readTimeout: Int,
    ): HttpURLConnection {
        val fullUrl = url + buildGetString(getParams)
        val connection = newData.proxy.connect(fullUrl).apply {
            this.requestMethod = requestMethod
            // force to false so we can handle it manually
            this.instanceFollowRedirects = false
            this.connectTimeout = connectTimeout
            this.readTimeout = readTimeout
        }

        return connection
    }

    override fun applyRequestProperties(con: HttpURLConnection, data: HttpData): HttpURLConnection {
        val requestCookies = data.cookies.filter { it.matches(con.url.toString()) && !it.hasExpired() }

        if (requestCookies.isNotEmpty()) {
            val cookieValue = requestCookies.joinToString("${Cookie.keyValDelimiter} ") { it.toKeyValue() }
            if (debuggingEnabled) println("Sending $cookieValue ${con.url}")

            con.setRequestProperty("Cookie", cookieValue)
        } else if (debuggingEnabled) {
            println("No cookies to send to ${con.url}")
        }

        for (header in data.headers) {
            con.setRequestProperty(header.name, header.toValueString())
        }

        return con
    }

    override fun handleRedirections(
        con: HttpURLConnection,
        followRedirects: Boolean,
        maxRedirects: Int,
        data: HttpData,
    ): HttpURLConnection {
        if (debuggingEnabled) println("Handling redirections for ${con.url}")

        var connection: HttpURLConnection = con
        var redirected: Boolean

        var counter = 0
        do {
            with(followRedirect(connection, data)) {
                //The cookie collector in Session.kt is implemented in this method, so we just call it ALWAYS,
                // but then immediately return the original connection
                if (!followRedirects) {
                    return con
                }

                if (debuggingEnabled) println("Followed redirect, new http con = $this")

                if (this != null) {
                    connection = this
                }

                redirected = this != null
            }

            if (counter == maxRedirects) {
                return connection
            }

            if (redirected) {
                counter++
            }
        } while (redirected)

        return connection
    }

    override fun followRedirect(con: HttpURLConnection, data: HttpData): HttpURLConnection? {
        if (debuggingEnabled) println("Following redirect for ${con.url}, responseCode=${con.responseCode}")
        if (!con.isHttpRedirect()) return null

        val location = con.getUrlLocation()
        if (location == null) return null

        val locationCon = data.proxy.connect(location).apply { instanceFollowRedirects = false }
        return applyRequestProperties(locationCon, data)
    }
}
