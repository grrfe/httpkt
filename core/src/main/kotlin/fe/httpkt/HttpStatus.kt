package fe.httpkt

public sealed class HttpStatus(public val code: Int) {
    public data object TooManyRequests : HttpStatus(429)
}

public fun HttpStatus.isMatch(statusCode: Int): Boolean {
    return code == statusCode
}
