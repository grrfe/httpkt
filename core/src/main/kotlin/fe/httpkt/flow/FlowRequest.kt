package fe.httpkt.flow

import fe.httpkt.Request
import fe.httpkt.Session

public class FlowFailedException(public val error: Any) : Exception(error.toString())

public interface FlowRequest<out S, in E : Any, R : Request> {
    @Throws(FlowFailedException::class)
    public fun execute(request: R): S

    public fun fail(failure: E): Nothing = throw FlowFailedException(failure)
}

public interface DataFlowRequest<in D, out S, in E : Any, R : Request> {
    @Throws(FlowFailedException::class)
    public fun execute(data: D, request: R): S
    public fun fail(failure: E): Nothing = throw FlowFailedException(failure)
}
