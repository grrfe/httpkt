package fe.httpkt

import java.net.HttpURLConnection

public typealias CustomSuccess = (HttpURLConnection) -> Boolean

public val HTTP_ERROR_RANGE: Pair<Int, Int> = Pair(HttpURLConnection.HTTP_BAD_REQUEST, 599)
public val HTTP_REDIRECT_STATUS_CODES: Array<Int> = arrayOf(
    HttpURLConnection.HTTP_MOVED_PERM,
    HttpURLConnection.HTTP_MOVED_TEMP,
    HttpURLConnection.HTTP_SEE_OTHER
)

public fun isHttpSuccess(code: Int): Boolean = code < HTTP_ERROR_RANGE.first || code > HTTP_ERROR_RANGE.second

public fun isHttpRedirect(code: Int): Boolean {
    return code in HTTP_REDIRECT_STATUS_CODES
}

public typealias ConnectionToType<T> = (HttpURLConnection) -> T

public sealed class HttpResult<T> {
    public class HttpSuccess<T>(public val value: T) : HttpResult<T>()
    public class HttpFailure<T>(public val con: HttpURLConnection) : HttpResult<T>()

    public fun getOrNull(): T? = if (this is HttpSuccess<T>) this.value else null

    public open fun getOrThrow(): T {
        return getOrNull()!!
    }

    public fun getOrDefault(default: T): T = if (this is HttpSuccess<T>) this.value else default

    public fun getOrElse(elseFn: ConnectionToType<T>): T = when (this) {
        is HttpSuccess<T> -> this.value
        is HttpFailure<T> -> elseFn.invoke(this.con)
    }

    public fun runOnError(handler: ConnectionToType<Unit>): HttpResult<T> {
        if (this is HttpFailure) handler.invoke(this.con)
        return this
    }
}
