package fe.httpkt


public sealed class HttpContentType(public val value: String) {
    public data object ApplicationJson : HttpContentType("application/json")
}

public fun HttpContentType.isMatch(headerValue: String): Boolean {
    val contentTypeValues = headerValue.split(";").map { it.trim() }
    return value in contentTypeValues
}
