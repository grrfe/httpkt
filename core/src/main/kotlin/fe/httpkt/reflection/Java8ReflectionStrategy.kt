package fe.httpkt.reflection

import java.lang.reflect.Field
import java.lang.reflect.Modifier
import kotlin.reflect.KClass

public object Java8ReflectionStrategy : ReflectionStrategy {
    private val modifiersField by lazy {
        Field::class.java.getDeclaredField("modifiers").apply { isAccessible = true }
    }

    private fun maybeRemoveFinalModifier(field: Field) {
        if (field.modifiers and Modifier.FINAL != 0) {
            modifiersField.setInt(field, field.modifiers and Modifier.FINAL.inv())
        }
    }

    private fun setStaticFieldValue(field: Field, value: Any?) {
        maybeRemoveFinalModifier(field)

        field.isAccessible = true
        field.set(null, value)
    }

    override fun setStaticFieldValue(clazz: KClass<*>, name: String, value: Any?) {
        val field = clazz.java.getDeclaredField(name)
        setStaticFieldValue(field, value)
    }
}
