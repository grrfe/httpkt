package fe.httpkt.reflection

import httpkt.com.google.gson.internal.JavaVersion
import java.net.HttpURLConnection
import kotlin.reflect.KClass

public interface ReflectionStrategy {
    public fun setStaticFieldValue(clazz: KClass<*>, name: String, value: Any?)
}

public object NoOpReflectionStrategy : ReflectionStrategy {
    override fun setStaticFieldValue(clazz: KClass<*>, name: String, value: Any?) {
    }
}

public interface ReflectionCoordinator {
    public fun tryPatchHttpMethods()
}

public object GlobalReflectionCoordinator : ReflectionCoordinator {
    public val strategy: ReflectionStrategy = when {
        !JavaVersion.isJava9OrLater -> Java8ReflectionStrategy
        else -> UnsafeReflectionStrategy
    }

    private val hasPatchedHttpMethods by lazy {
        strategy.setStaticFieldValue(
            HttpURLConnection::class,
            "methods",
            arrayOf("GET", "POST", "HEAD", "OPTIONS", "PUT", "DELETE", "TRACE", "PATCH")
        )
    }

    public override fun tryPatchHttpMethods() {
        return hasPatchedHttpMethods
    }
}
