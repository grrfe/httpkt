package fe.httpkt.reflection

import sun.misc.Unsafe
import java.lang.reflect.Field
import kotlin.reflect.KClass

public object UnsafeReflectionStrategy : ReflectionStrategy {
    private val unsafe by lazy {
        val field = Unsafe::class.java.getDeclaredField("theUnsafe").apply { isAccessible = true }
        val unsafe = field.get(null) as Unsafe

        unsafe
    }

    private fun setStaticFieldValue(field: Field, value: Any?) {
        unsafe.putObject(unsafe.staticFieldBase(field), unsafe.staticFieldOffset(field), value)
    }

    override fun setStaticFieldValue(clazz: KClass<*>, name: String, value: Any?) {
        val field = clazz.java.getDeclaredField(name)
        setStaticFieldValue(field, value)
    }
}
