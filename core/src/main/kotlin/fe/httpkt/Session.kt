package fe.httpkt

import fe.httpkt.body.BodyData
import fe.httpkt.body.EmptyBody
import fe.httpkt.data.Cookie
import fe.httpkt.ext.cookies
import fe.httpkt.internal.HttpInternals
import fe.httpkt.internal.RealHttpInternals
import java.net.HttpURLConnection

/**
 * An implementation of [Request] which allows the developer to persist cookies between requests. All cookies received
 * by any request sent using an instance of [Session] are retained on that instance and are used for all subsequent requests.
 */
public open class Session(
    private val sessionCookies: HttpData.Builder = HttpData.Builder(),
    internals: HttpInternals = RealHttpInternals(),
    instanceData: HttpData = HttpData.default,
) : Request(internals, instanceData), HttpInternals by internals {

    public constructor(data: HttpDataBuilder) : this(instanceData = HttpData.of(data))
    public constructor(data: HttpData.Builder) : this(instanceData = data.build())
    public constructor(data: HttpData) : this(instanceData = data)

    public fun getSessionCookies(): MutableSet<Cookie> = sessionCookies.cookies

    private fun mergeData(reqData: HttpData?): HttpData {
        val merged = this.instanceData.merge(this.sessionCookies.build(), HttpData.MergeType.Cookies)
        return with(merged) {
            if (reqData != null) this + reqData else this
        }
    }

    override fun update(data: HttpData): HttpData {
        this.instanceData += data
        this.sessionCookies.mergeCookies(data)

        return this.instanceData
    }

    override fun update(builder: HttpDataBuilder): HttpData {
        return update(HttpData.of(builder))
    }

    /**
     * Sends a get request
     *
     * @param url the url to send the request to
     * @param getParams a map of get key-value pairs which are just there for convenience so parameters like "?key=val1&key2=val2" don't have to be included in the url string
     * @param dataBuilder function consuming an [HttpData.Builder] which allows the developer to build an HttpData object which, when the function is non-null,
     * overrides the proxy/cookie/headers of the main [dataBuilder] object (cookies/headers are only overridden when a cookie/header
     * with the same name is present in the main [dataBuilder] object, the proxy is overridden if it is different)
     * @param followRedirects determines if the [HttpURLConnection] should follow redirects (default = true)
     * @param forceSend determines if the request should be forcefully sent by calling [HttpURLConnection.getInputStream] so you don't have to (default = false)
     *
     * @return a [HttpURLConnection] object
     */
    override fun getFn(
        url: String,
        getParams: Map<Any, Any>,
        dataBuilder: (HttpData.Builder.() -> Unit)?,
        followRedirects: Boolean,
        forceSend: Boolean,
    ): HttpURLConnection = this.get(
        url,
        getParams = getParams,
        data = dataBuilder?.let { HttpData.of(it) },
        followRedirects = followRedirects,
        forceSend = forceSend
    )


    /**
     * Sends a get request
     *
     * @param url the url to send the request to
     * @param getParams a map of get key-value pairs which are just there for convenience so parameters like "?key=val1&key2=val2" don't have to be included in the url string
     * @param data a [HttpData] object (default = null) which, when non-null, overrides the proxy/cookie/headers of the main [data] object (cookies/headers are only
     * overridden when a cookie/header with the same name is present in the main [data] object, the proxy is overridden if it is different)
     * @param followRedirects determines if the [HttpURLConnection] should follow redirects (default = true)
     * @param forceSend determines if the request should be forcefully sent by calling [HttpURLConnection.getInputStream] so you don't have to (default = false)
     *
     * @return a [HttpURLConnection] object
     */
    override fun get(
        url: String,
        getParams: Map<Any, Any>,
        data: HttpData?,
        connectTimeout: Int,
        readTimeout: Int,
        followRedirects: Boolean,
        maxRedirects: Int,
        forceSend: Boolean,
    ): HttpURLConnection = super.get(
        url,
        getParams = getParams,
        data = mergeData(data),
        connectTimeout = connectTimeout, readTimeout = readTimeout,
        followRedirects = followRedirects,
        maxRedirects = maxRedirects,
        forceSend = forceSend
    )

    /**
     * Sends non-get requests
     *
     * @param method the type of request to send
     * @param url the url to send the request to
     * @param body the body data to send to the server, default is [EmptyBody] which means no bodydata is sent
     * @param dataBuilder function consuming an [HttpData.Builder] which allows the developer to build an HttpData object which, when the function is non-null,
     * overrides the proxy/cookie/headers of the main [dataBuilder] object (cookies/headers are only overridden when a cookie/header
     * with the same name is present in the main [dataBuilder] object, the proxy is overridden if it is different)
     * @param followRedirects determines if the [HttpURLConnection] should follow redirects (default = true)
     * @param forceSend determines if the request should be forcefully sent by calling [HttpURLConnection.getInputStream] so you don't have to (default = false)
     *
     * @return a [HttpURLConnection] object
     */
    override fun requestFn(
        method: HttpMethod,
        url: String,
        body: BodyData,
        dataBuilder: HttpDataBuilder?,
        connectTimeout: Int,
        readTimeout: Int,
        followRedirects: Boolean,
        forceSend: Boolean,
    ): HttpURLConnection = this.request(
        method,
        url,
        body = body,
        data = dataBuilder?.let { HttpData.of(it) },
        connectTimeout = connectTimeout, readTimeout = readTimeout,
        followRedirects = followRedirects,
        forceSend = forceSend
    )

    /**
     * Sends non-get requestsfirstDelimiterIdx
     *
     * @param method the type of request to send
     * @param url the url to send the request to
     * @param body the body data to send to the server, default is [EmptyBody] which means no bodydata is sent
     * @param data a [HttpData] object (default = null) which, when non-null, overrides the proxy/cookie/headers of the main [data] object (cookies/headers are only
     * overridden when a cookie/header with the same name is present in the main [data] object, the proxy is overridden if it is different)
     * @param followRedirects determines if the [HttpURLConnection] should follow redirects (default = true)
     * @param forceSend determines if the request should be forcefully sent by calling [HttpURLConnection.getInputStream] so you don't have to (default = false)
     *
     * @return a [HttpURLConnection] object
     */
    override fun request(
        method: HttpMethod,
        url: String,
        body: BodyData,
        data: HttpData?,
        connectTimeout: Int,
        readTimeout: Int,
        followRedirects: Boolean,
        maxRedirects: Int,
        forceSend: Boolean,
    ): HttpURLConnection = super.request(
        method, url,
        body = body,
        data = mergeData(data),
        connectTimeout = connectTimeout, readTimeout = readTimeout,
        followRedirects = followRedirects,
        maxRedirects = maxRedirects,
        forceSend = forceSend
    )

    override fun followRedirect(con: HttpURLConnection, data: HttpData): HttpURLConnection? {
        sessionCookies.addCookie(*con.cookies().toTypedArray().also {
            if (debuggingEnabled) println("Collected ${it.contentToString()} for ${con.url}")
        })

        return internals.followRedirect(con, this.mergeData(data))
    }
}
