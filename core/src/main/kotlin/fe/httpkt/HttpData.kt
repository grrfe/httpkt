package fe.httpkt

import fe.httpkt.data.Cookie
import fe.httpkt.data.Header
import fe.httpkt.data.HeaderDslNode
import fe.httpkt.proxy.AuthProxy


public typealias HttpDataBuilder = HttpData.Builder.() -> Unit


/**
 * A class which encapsulates an [AuthProxy] as well as a list for [Header]s and one for [Cookie]s
 * The "data" encapsulated by one instance of this class is sent to the server when requests are made through [Request]
 *
 * @param proxy the proxy that should be used, defaults to an "empty" [AuthProxy] which means no proxy will be used
 * @param headers a list of [Header]s to use for requests, defaults to an empty list
 * @param cookies a list of [Cookie]s to use for requests, defaults to an empty list
 */
public data class HttpData private constructor(
    var proxy: AuthProxy = AuthProxy(),
    val headers: Set<Header> = setOf(),
    val cookies: Set<Cookie> = setOf(),
    val proxyState: Builder.ProxyState = Builder.ProxyState.Keep,
    val removeHeaders: Set<String> = setOf(),
    val removeCookies: Set<String> = setOf(),
) {
    public companion object {
        @JvmField
        public val default: HttpData = Builder().build()
        public fun of(builder: HttpDataBuilder): HttpData = Builder().apply(builder).build()
    }

    public class Builder(
        public var proxy: AuthProxy = AuthProxy(),
        public val headers: MutableSet<Header> = mutableSetOf(),
        public val cookies: MutableSet<Cookie> = mutableSetOf(),
        public var proxyState: ProxyState = ProxyState.Keep,
        private val removeExistingHeader: MutableSet<String> = mutableSetOf(),
        private val removeCookies: MutableSet<String> = mutableSetOf(),
    ) {
        public enum class ProxyState {
            Keep, Remove, Replace
        }

        public companion object {
            public fun import(httpData: HttpData): Builder {
                return Builder().apply {
                    this.proxy = httpData.proxy
                    this.headers.addAll(httpData.headers)
                    this.cookies.addAll(httpData.cookies)
                    this.removeExistingHeader.addAll(httpData.removeHeaders)
                    this.removeCookies.addAll(httpData.removeCookies)
                }
            }
        }

        public inline fun headers(fn: HeaderDslNode.() -> Unit): Builder {
            val headers = HeaderDslNode().apply(fn).build()
            for (header in headers) {
                addHeader(header)
            }

            return this
        }

        public fun addHeader(vararg headers: Header): Builder {
            headers.forEach { this.addHeader(it) }
            return this
        }

        @Deprecated(message = "Use new API", replaceWith = ReplaceWith("addHeader(name, values)"))
        public fun addHeaderImpl(name: String, vararg values: String): Builder {
            this.addHeader(Header(name, *values))
            return this
        }

        public fun addHeaders(list: List<Header>): Builder {
            list.forEach { addHeader(it) }
            return this
        }

        // Add Header
        public fun addHeader(header: Header): Builder {
            if (!this.headers.add(header)) {
                this.headers.remove(header)
                this.headers.add(header)
            }

            return this
        }

        public fun addHeader(name: String, vararg values: String): Builder {
            return addHeader(Header(name, *values))
        }

        public fun addHeader(vararg headers: Pair<String, String>): Builder {
            for ((name, value) in headers) {
                addHeader(name, value)
            }

            return this
        }

        public fun removeExistingHeader(name: String): Builder {
            this.removeExistingHeader.add(name)
            return this
        }

        public fun findHeaderByName(name: String): Header? = headers.find {
            it.name == name
        }

        public fun addCookie(vararg cookies: Cookie): Builder {
            cookies.forEach {
                this.addCookie(it)
            }

            return this
        }

        public fun addCookie(name: String, value: String): Builder {
            this.addCookie(Cookie.of(name, value))
            return this
        }

        public fun addCookie(cookie: Cookie): Builder {
            if (!this.cookies.add(cookie)) {
                this.cookies.remove(cookie)
                this.cookies.add(cookie)
            }

            return this
        }

        public fun removeCookie(name: String): Builder {
            this.removeCookies.add(name)
            return this
        }

        public fun findCookieByName(name: String): Cookie? = cookies.find {
            it.name == name
        }

        public fun mergeHeaders(props: HttpData): Builder {
            with(mutableMapOf<String, Header>()) {
                listOf(headers, props.headers).flatten().forEach {
                    if (it.name !in props.removeHeaders) {
                        this[it.name] = it
                    }
                }

                headers.clear()
                headers.addAll(this.values)
            }

            return this
        }

//        public fun mergeHeaders(httpData: HttpData, props: HttpData): Builder {
//            with(mutableMapOf<String, Header>()) {
//                listOf(httpData.headers, props.headers).flatten().forEach {
//                    if (it.name !in props.removeHeaders) {
//                        this[it.name] = it
//                    }
//                }
//
//                headers += this.values
//            }
//
//            return this
//        }

        public fun mergeCookies(props: HttpData): Builder {
            with(mutableMapOf<String, Cookie>()) {
                listOf(cookies, props.cookies).flatten().forEach {
                    if (it.name !in props.removeCookies) {
                        this[it.name] = it
                    }
                }

                cookies.clear()
                cookies.addAll(this.values)
            }

            return this
        }

        public fun mergeProxy(props: HttpData): Builder {
            if (props.proxyState == ProxyState.Remove) {
                proxy = AuthProxy()
            } else if (props.proxyState == ProxyState.Replace) {
                if (props.proxy != proxy) {
                    proxy = props.proxy
                }
            }

            return this
        }

        public fun build(): HttpData = HttpData(proxy, headers, cookies, proxyState, removeExistingHeader, removeCookies)
    }

    /**
     * Implements Kotlin's plus operation for this object, so that two instances of [HttpData] can be easily combined.
     * The headers/cookies/proxy of the passed parameter have priority; For example, if the passed [HttpData] object
     * encapsulates a [Header] which has the same name as a [Header] encapsulated by this instance (on which plus() is called),
     * then the [Header] of the passed [props] parameter is used.
     * The same is also true for cookies; When the proxy is different, the one of [props] is used.
     *
     * @param props the [HttpData] which should be combined with this one
     */
    public operator fun plus(props: HttpData): HttpData {
        return merge(props, MergeType.All)
    }

    public fun merge(props: HttpData, mergeType: MergeType = MergeType.All): HttpData {
        val builder = Builder.import(this)
        if (mergeType == MergeType.Headers || mergeType == MergeType.All) {
            builder.mergeHeaders(props)
        }

        if (mergeType == MergeType.Cookies || mergeType == MergeType.All) {
            builder.mergeCookies(props)
        }

        if (mergeType == MergeType.Proxy || mergeType == MergeType.All) {
            builder.mergeProxy(props)
        }

        return builder.build()
    }

    public enum class MergeType {
        All, Headers, Cookies, Proxy;
    }
}

