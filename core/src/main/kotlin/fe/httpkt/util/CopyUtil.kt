package fe.httpkt.util

import java.io.InputStream
import java.io.OutputStream

public typealias CopyStatus = (bytesCopied: Long) -> Unit

/**
 * Copied over [InputStream#copyTo] extension function and added the ability to provide
 * status-handler which is invoked everytime the buffer read from [instream] is written to [outstream].
 *
 * **Note** It is the caller's responsibility to close both of these resources.
 */
public fun copyTo(
    instream: InputStream,
    outstream: OutputStream,
    bufferSize: Int,
    statusUpdate: CopyStatus? = null
): Long {
    var bytesCopied = 0L
    val buffer = ByteArray(bufferSize)

    var bytes = instream.read(buffer)
    while (bytes >= 0) {
        outstream.write(buffer, 0, bytes)
        bytesCopied += bytes
        statusUpdate?.invoke(bytesCopied)
        bytes = instream.read(buffer)
    }

    return bytesCopied
}
