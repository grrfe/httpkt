package fe.httpkt.util

import fe.httpkt.data.Header


public fun headersOf(vararg pairs: Pair<String, String>): List<Header> {
    return pairs.map { (key, value) -> Header(key, value) }
}
