package fe.httpkt.util

import java.io.InputStream
import java.net.HttpURLConnection

/**
 * Util which determines and passes the correct [Default] to the developer, so they do not have to worry about
 * checking which [Default] should be used. According to [https://stackoverflow.com/a/24986433](this SO answer),
 * [HttpURLConnection.getErrorStream] is only used for the status-codes 401 and 407
 */
public enum class StreamType {
    Auto {
        override fun retrieve(con: HttpURLConnection) = decide(con).retrieve(con)
    },
    Default {
        override fun retrieve(con: HttpURLConnection): InputStream = con.inputStream
    },
    Error {
        override fun retrieve(con: HttpURLConnection): InputStream = con.errorStream
    };

    public abstract fun retrieve(con: HttpURLConnection): InputStream

    public companion object {
        /**
         * Decides which [StreamType] to use from a given [HttpURLConnection]
         *
         * @param con the [HttpURLConnection] to get the [StreamType] from
         * @return returns the correct [StreamType]
         */
        public fun decide(con: HttpURLConnection): StreamType {
            return if (con.errorStream == null) Default else Error
        }
    }
}
