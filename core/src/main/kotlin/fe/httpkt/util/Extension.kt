package fe.httpkt.util

import java.net.URL


public object Extension {
    private const val protocolIndicator = "://"
    public const val setCookieHeader: String = "Set-Cookie"

    private val tlds = lazy {
        Extension::class.java.getResourceAsStream("/tlds.txt")?.bufferedReader()?.readLines() ?: emptyList()
    }

    public fun getFileNameFromUrlPath(path: String?): Pair<String?, String?> {
        if (path.isNullOrEmpty()) {
            return null to null
        }

        val slashIndex = path.lastIndexOf("/")
        if (slashIndex == -1 || slashIndex == path.length - 1) {
            return null to null
        }

        val file = path.substring(slashIndex + 1)

        val extIndex = file.lastIndexOf(".")
        return if (extIndex > -1 && extIndex < file.length - 1) {
            val name = file.substring(0, extIndex)
            val ext = file.substring(extIndex + 1)

            name to ext
        } else file to null
    }

    public fun getFileNameFromUrl(url: URL): Pair<String?, String?> = getFileNameFromUrlPath(url.path)


    public fun getProtocol(url: String): String {
        return if (url.contains(protocolIndicator)) {
            url.substring(0, url.indexOf(protocolIndicator) + protocolIndicator.length)
        } else url
    }

    public fun stripProtocol(url: String): String {
        return if (url.contains(protocolIndicator)) {
            url.substring(url.indexOf(protocolIndicator) + protocolIndicator.length)
        } else url
    }

    public fun urlToDomain(url: String): String {
        return substringUntil(stripProtocol(url), "/")
    }

    public fun splitDomain(domain: String): Pair<String, List<String>> {
        val tld = tlds.value.find { domain.endsWith(it) }
        if (tld != null) {
            val domainWithoutTld = domain.substring(0, domain.indexOf(tld))
            val lastDot = domainWithoutTld.lastIndexOf(".")
            return if (lastDot != -1) {
                if (lastDot == 0) {
                    //.google.com
                    domain.substring(1) to emptyList()
                } else {
                    //test.google.com
                    val mainDomain = domain.substring(lastDot + 1)
                    val subDomains = domain.substring(0, lastDot).split(".").reversed()

                    mainDomain to subDomains
                }
            } else {
                //google.com
                domain to emptyList()
            }
        }

        return domain to emptyList()
    }

    private fun substringUntil(str: String, till: String) = with(str.indexOf(till)) {
        if (this != -1) {
            str.substring(0, this)
        } else str
    }
}

