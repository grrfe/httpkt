package fe.httpkt.stream

import java.net.HttpURLConnection

public val HttpURLConnection.isGzipped: Boolean
    get() = contentEncoding?.contains("gzip") == true
