package fe.httpkt

import fe.httpkt.body.BodyData
import fe.httpkt.body.EmptyBody
import fe.httpkt.clazz.ClassRequest
import fe.httpkt.data.HEADER_CONTENT_TYPE
import fe.httpkt.ext.setOnCustomSuccess
import fe.httpkt.internal.HttpInternals
import fe.httpkt.internal.RealHttpInternals
import fe.httpkt.internal.forceSend
import fe.httpkt.reflection.GlobalReflectionCoordinator
import java.io.IOException
import java.net.HttpURLConnection

/**
 * The main request class. The constructor has two parameters:
 *
 * @param instanceData an [HttpData] object which encapsulates the current proxy, headers and cookies; Data present on this
 * object is sent with every request of this [Request] instance
 */
public open class Request(
    protected val internals: HttpInternals = RealHttpInternals(),
    protected var instanceData: HttpData = HttpData.default,
) {
    public constructor(data: HttpDataBuilder) : this(instanceData = HttpData.of(data))
    public constructor(data: HttpData.Builder) : this(instanceData = data.build())
    public constructor(data: HttpData) : this(instanceData = data)

    public fun data(): HttpData = instanceData

    public open fun update(data: HttpData): HttpData {
        return mergeData(data)
    }

    public open fun update(builder: HttpDataBuilder): HttpData {
        return update(HttpData.of(builder))
    }

    /**
     * Sends the given [ClassRequest] using this [Request] instance
     *
     * @param classRequest the [ClassRequest] to send
     * @throws ClassRequest.ClassRequestFailedException if the request failed; the instance encapsulates the error provided by the [ClassRequest];
     * success/non-success using [ClassRequest.customSuccess] if non-null or by the response code being in the range of [HTTP_ERROR_RANGE] (inclusive)
     * @return the success value provided by the [ClassRequest] instance
     */
    @Throws(ClassRequest.ClassRequestFailedException::class)
    public fun <S, E : Any> send(classRequest: ClassRequest<S, E>): S {
        val con = classRequest.send(this)

        if (con.setOnCustomSuccess(classRequest.customSuccess).second) {
            return classRequest.handleSuccess(con)
        }

        throw ClassRequest.ClassRequestFailedException(classRequest.handleError(con))
    }

    private fun mergeData(data: HttpData?) = if (data != null) this.instanceData + data else this.instanceData

    /**
     * Sends a get request
     *
     * @param url the url to send the request to
     * @param getParams a map of get key-value pairs which are just there for convenience so parameters like "?key=val1&key2=val2" don't have to be included in the url string
     * @param dataBuilder function consuming an [HttpData.Builder] which allows the developer to build an HttpData object which, when the function is non-null,
     * overrides the proxy/cookie/headers of the main [dataBuilder] object (cookies/headers are only overridden when a cookie/header
     * with the same name is present in the main [dataBuilder] object, the proxy is overridden if it is different)
     * @param followRedirects determines if the [HttpURLConnection] should follow redirects (default = true)
     * @param forceSend determines if the request should be forcefully sent by calling [HttpURLConnection.getInputStream] so you don't have to (default = false)
     * @throws IOException if an I/O exception occurs
     *
     * @return a [HttpURLConnection] object
     */
    @Throws(IOException::class)
    public open fun getFn(
        url: String,
        getParams: Map<Any, Any> = emptyMap(),
        dataBuilder: HttpDataBuilder? = null,
        followRedirects: Boolean = true,
        forceSend: Boolean = false,
    ): HttpURLConnection = get(
        url,
        getParams = getParams,
        data = dataBuilder?.let { HttpData.of(it) },
        followRedirects = followRedirects,
        forceSend = forceSend
    )

    /**
     * Sends a get request
     *
     * @param url the url to send the request to
     * @param getParams a map of get key-value pairs which are just there for convenience so parameters like "?key=val1&key2=val2" don't have to be included in the url string
     * @param data a [HttpData] object (default = null) which, when non-null, overrides the proxy/cookie/headers of the main [data] object (cookies/headers are only
     * overridden when a cookie/header with the same name is present in the main [data] object, the proxy is overridden if it is different)
     * @param followRedirects determines if the [HttpURLConnection] should follow redirects (default = true)
     * @param forceSend determines if the request should be forcefully sent by calling [HttpURLConnection.getInputStream] so you don't have to (default = false)
     * @throws IOException if an I/O exception occurs
     *
     * @return a [HttpURLConnection] object
     */
    @Throws(IOException::class)
    public open fun get(
        url: String,
        getParams: Map<Any, Any> = emptyMap(),
        data: HttpData? = null,
        connectTimeout: Int = 0,
        readTimeout: Int = 0,
        followRedirects: Boolean = true,
        maxRedirects: Int = 5,
        forceSend: Boolean = false,
    ): HttpURLConnection {
        val newData = mergeData(data)
        return noBodyRequest(
            "GET",
            url,
            getParams,
            newData,
            connectTimeout,
            readTimeout,
            followRedirects,
            maxRedirects,
            forceSend
        )
    }

    @Throws(IOException::class)
    public open fun head(
        url: String,
        getParams: Map<Any, Any> = emptyMap(),
        data: HttpData? = null,
        connectTimeout: Int = 0,
        readTimeout: Int = 0,
        followRedirects: Boolean = true,
        maxRedirects: Int = 5,
        forceSend: Boolean = false,
    ): HttpURLConnection {
        val newData = mergeData(data)
        return noBodyRequest(
            "HEAD",
            url,
            getParams,
            newData,
            connectTimeout,
            readTimeout,
            followRedirects,
            maxRedirects,
            forceSend
        )
    }

    private fun noBodyRequest(
        requestMethod: String,
        url: String,
        getParams: Map<Any, Any> = emptyMap(),
        data: HttpData,
        connectTimeout: Int = 0,
        readTimeout: Int = 0,
        followRedirects: Boolean = true,
        maxRedirects: Int = 5,
        forceSend: Boolean = false,
    ): HttpURLConnection {
        val con = internals.setupConnection(requestMethod, url, getParams, data, connectTimeout, readTimeout)

//        if (debuggingEnabled) {
//            println("Request with data: $data")
//            println("InstanceData: $instanceData")
//            println("MergedData: $newData")
//        }

        return internals.applyRequestProperties(con, data)
            .let { internals.handleRedirections(it, followRedirects, maxRedirects, data) }
            .forceSend(forceSend)
    }


    /**
     * Sends non-get requests
     *
     * @param method the type of request to send
     * @param url the url to send the request to
     * @param body the body data to send to the server, default is [EmptyBody] which means no body-data is sent
     * @param dataBuilder function consuming an [HttpData.Builder] which allows the developer to build an HttpData object which, when the function is non-null,
     * overrides the proxy/cookie/headers of the main [dataBuilder] object (cookies/headers are only overridden when a cookie/header
     * with the same name is present in the main [dataBuilder] object, the proxy is overridden if it is different)
     * @param followRedirects determines if the [HttpURLConnection] should follow redirects (default = true)
     * @param forceSend determines if the request should be forcefully sent by calling [HttpURLConnection.getInputStream] so you don't have to (default = false)
     * @throws IOException if an I/O exception occurs
     *
     * @return a [HttpURLConnection] object
     */
    @Throws(IOException::class)
    public open fun requestFn(
        method: HttpMethod,
        url: String,
        body: BodyData = EmptyBody(),
        dataBuilder: HttpDataBuilder? = null,
        connectTimeout: Int = 0,
        readTimeout: Int = 0,
        followRedirects: Boolean = true,
        forceSend: Boolean = false,
    ): HttpURLConnection = request(
        method,
        url,
        body = body,
        data = dataBuilder?.let { HttpData.of(it) },
        connectTimeout = connectTimeout,
        readTimeout = readTimeout,
        followRedirects = followRedirects,
        forceSend = forceSend
    )

    @Throws(IOException::class)
    public open fun request(
        method: HttpMethod,
        url: String,
        body: BodyData = EmptyBody(),
        data: HttpData? = null,
        connectTimeout: Int = 0,
        readTimeout: Int = 0,
        followRedirects: Boolean = true,
        maxRedirects: Int = 5,
        forceSend: Boolean = false,
    ): HttpURLConnection {
        if (method == HttpMethod.Patch) {
            GlobalReflectionCoordinator.tryPatchHttpMethods()
        }

        val newData = mergeData(data)
        val con = internals.setupConnection(
            method.toString().uppercase(),
            url,
            emptyMap(),
            newData,
            connectTimeout,
            readTimeout
        )
        con.doOutput = true

        if (debuggingEnabled) {
            println("Request with data: $data")
            println("InstanceData: $instanceData")
            println("MergedData: $newData")
        }

        val emptyBody = (body is EmptyBody)
        if (!emptyBody) {
            con.setRequestProperty(HEADER_CONTENT_TYPE, body.name)
        }

        with(body.processContent()) {
            if (this != -1L) {
                con.setFixedLengthStreamingMode(this)
            }
        }

        internals.applyRequestProperties(con, newData)
        con.useCaches = false
        con.connect()

        if (!emptyBody) {
            body.writeData(con.outputStream)
        }

        return internals.handleRedirections(con, followRedirects, maxRedirects, newData).forceSend(forceSend)
    }

    @Throws(IOException::class)
    public open fun post(
        url: String,
        body: BodyData = EmptyBody(),
        dataBuilder: HttpDataBuilder? = null,
        connectTimeout: Int = 0,
        readTimeout: Int = 0,
        followRedirects: Boolean = true,
        forceSend: Boolean = false,
    ): HttpURLConnection = request(
        HttpMethod.Post,
        url,
        body,
        data = dataBuilder?.let { HttpData.of(it) },
        connectTimeout = connectTimeout,
        readTimeout = readTimeout,
        followRedirects = followRedirects,
        forceSend = forceSend
    )

    @Throws(IOException::class)
    public open fun put(
        url: String,
        body: BodyData = EmptyBody(),
        dataBuilder: (HttpData.Builder.() -> Unit)? = null,
        connectTimeout: Int = 0,
        readTimeout: Int = 0,
        followRedirects: Boolean = true,
        forceSend: Boolean = false,
    ): HttpURLConnection = request(
        HttpMethod.Put,
        url,
        body,
        data = dataBuilder?.let { HttpData.of(it) },
        connectTimeout = connectTimeout,
        readTimeout = readTimeout,
        followRedirects = followRedirects,
        forceSend = forceSend
    )

    @Throws(IOException::class)
    public open fun patch(
        url: String,
        body: BodyData = EmptyBody(),
        dataBuilder: HttpDataBuilder? = null,
        connectTimeout: Int = 0,
        readTimeout: Int = 0,
        followRedirects: Boolean = true,
        forceSend: Boolean = false,
    ): HttpURLConnection = request(
        HttpMethod.Patch,
        url,
        body,
        data = dataBuilder?.let { HttpData.of(it) },
        connectTimeout = connectTimeout, readTimeout = readTimeout,
        followRedirects = followRedirects,
        forceSend = forceSend
    )

    @Throws(IOException::class)
    public open fun delete(
        url: String,
        body: BodyData = EmptyBody(),
        dataBuilder: HttpDataBuilder? = null,
        connectTimeout: Int = 0,
        readTimeout: Int = 0,
        followRedirects: Boolean = true,
        forceSend: Boolean = false,
    ): HttpURLConnection = request(
        HttpMethod.Delete,
        url,
        body,
        data = dataBuilder?.let { HttpData.of(it) },
        connectTimeout = connectTimeout, readTimeout = readTimeout,
        followRedirects = followRedirects,
        forceSend = forceSend
    )

    public enum class HttpMethod {
        Post, Put, Patch, Delete, Options
    }
}
