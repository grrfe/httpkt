package fe.httpkt.clazz

import fe.httpkt.HttpData
import fe.httpkt.Request
import fe.httpkt.body.BodyData
import fe.httpkt.body.EmptyBody
import fe.httpkt.CustomSuccess
import java.net.HttpURLConnection

/**
 * An abstract class defining the structure of a class request
 *
 * @param url the url the request should be sent to
 * @param customSuccess may be passed to change the default way a "success" is determined
 */
public abstract class ClassRequest<S, E : Any>(public val url: String, public val customSuccess: CustomSuccess? = null) {
    public class ClassRequestFailedException(public val error: Any) : Exception()

    public abstract fun send(request: Request): HttpURLConnection

    /**
     * Invoked by [Request.send] if the request has been determined to be a success
     */
    public abstract fun handleSuccess(con: HttpURLConnection): S

    /**
     * Invoked by [Request.send] if the request has been determined to be an error
     */
    public abstract fun handleError(con: HttpURLConnection): E
}

/**
 * An implementation of [ClassRequest] allowing the user to implement their own GET request classes
 *
 * @param url the url to send the request to
 * @param getParams a map of get key-value pairs which are just there for convenience so parameters like "?key=val1&key2=val2" don't have to be included in the url string
 * @param data a [HttpData] object (default = null) which, when non-null, overrides the proxy/cookie/headers of the main [data] object (cookies/headers are only
 * overridden when a cookie/header with the same name is present in the main [data] object, the proxy is overridden if it is different)
 * @param followRedirects determines if the [HttpURLConnection] should follow redirects (default = true)
 * @param forceSend determines if the request should be forcefully sent by calling [HttpURLConnection.getInputStream] so you don't have to (default = false)
 */
public abstract class GetRequest<S, E : Any>(
    url: String,
    public val getParams: Map<Any, Any> = emptyMap(),
    public val data: (HttpData.Builder.() -> Unit)? = null,
    public val connectTimeout: Int = 0,
    public val readTimeout: Int = 0,
    public val followRedirects: Boolean = true,
    public val maxRedirects: Int = 5,
    public val forceSend: Boolean = false,
    customSuccess: CustomSuccess? = null
) : ClassRequest<S, E>(url, customSuccess) {

    /**
     * Invokes [Request.send] using the parameters passed to the constructor
     *
     * @return a [HttpURLConnection] object
     */
    override fun send(request: Request): HttpURLConnection {
        return request.get(
            url,
            getParams,
            data?.let { HttpData.of(it) },
            connectTimeout,
            readTimeout,
            followRedirects,
            maxRedirects,
            forceSend
        )
    }
}

/**
 * An implementation of [ClassRequest] allowing the user to implement their own non-GET request classes
 *
 * @param method the type of request to send
 * @param url the url to send the request to
 * @param body the body data to send to the server, default is [EmptyBody] which means no bodydata is sent
 * @param data a [HttpData] object (default = null) which, when non-null, overrides the proxy/cookie/headers of the main [data] object (cookies/headers are only
 * overridden when a cookie/header with the same name is present in the main [data] object, the proxy is overridden if it is different)
 * @param followRedirects determines if the [HttpURLConnection] should follow redirects (default = true)
 * @param forceSend determines if the request should be forcefully sent by calling [HttpURLConnection.getInputStream] so you don't have to (default = false)
 *
 * @return a [HttpURLConnection] object
 */
public abstract class BodyRequest<S, E : Any>(
    public val method: Request.HttpMethod,
    url: String,
    private val body: BodyData = EmptyBody(),
    public val data: (HttpData.Builder.() -> Unit)? = null,
    public val connectTimeout: Int = 0,
    public val readTimeout: Int = 0,
    public val followRedirects: Boolean = true,
    public val maxRedirects: Int = 5,
    public val forceSend: Boolean = false,
    customSuccess: CustomSuccess? = null
) : ClassRequest<S, E>(url, customSuccess) {

    /**
     * Invokes [Request.send] using the parameters passed to the constructor
     *
     * @return a [HttpURLConnection] object
     */
    override fun send(request: Request): HttpURLConnection {
        return request.request(
            method,
            url,
            body,
            data?.let { HttpData.of(it) },
            connectTimeout,
            readTimeout,
            followRedirects,
            maxRedirects,
            forceSend
        )
    }
}
