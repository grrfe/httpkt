package fe.httpkt.proxy

import java.io.IOException
import java.net.*

/**
 * A class which encapsulates a [Proxy] object as well as a [ProxyLogin] object, since Java does not seem to support proxy authentication
 * @param proxy the [Proxy] to use, defaults to [Proxy.NO_PROXY]
 * @param login the [ProxyLogin] to use (if any), defaults to null
 */
public data class AuthProxy(val proxy: Proxy? = Proxy.NO_PROXY, val login: ProxyLogin? = null) {

    public constructor(type: Proxy.Type, hostname: String, port: Int, login: ProxyLogin? = null) : this(
        Proxy(type, InetSocketAddress(hostname, port)), login
    )

    /**
     * Open a connection the given url through this proxy
     *
     * @param url the url to connect to
     * @throws IOException if an I/O exception occurs
     */
    @Throws(IOException::class)
    public fun connect(url: String): HttpURLConnection {
        return URL(url).openConnection(prepare()) as HttpURLConnection
    }

    public fun prepare(): Proxy {
        login?.let {
            Authenticator.setDefault(it.authenticator)
        }

        return proxy ?: Proxy.NO_PROXY
    }
}

/**
 * Implements [ProxyLogin] to provide the ability to log into a proxy using a username/password combo
 *
 * @param username the username to use for login
 * @param password the password to use for login
 */
public data class PasswordProxyLogin(val username: String, val password: String) : ProxyLogin {

    override val authenticator: Authenticator = object : Authenticator() {
        override fun getPasswordAuthentication(): PasswordAuthentication {
            return PasswordAuthentication(username, password.toCharArray())
        }
    }
}

/**
 * An interface which makes it possible to implement different proxy-auth methods
 */
public interface ProxyLogin {
    public val authenticator: Authenticator
}
