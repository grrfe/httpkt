package fe.httpkt.body

import java.io.OutputStream
import java.net.URLEncoder
import kotlin.experimental.ExperimentalTypeInference

/**
 * An implementation of [BodyData] allowing the user to send data by "form-url-encoding" it (application/x-www-form-urlencoded).
 * @param data the data that should be sent to the server formurlencoded
 */
public class FormUrlEncodedBody(private val data: Map<String, Any>) : BodyData("application/x-www-form-urlencoded") {
    private lateinit var bytes: ByteArray

    public constructor(vararg data: Pair<String, Any>) : this(mapOf(*data))

    override fun processContent(): Long {
        var i = 0
        return buildString {
            data.forEach { (key, value) ->
                if (i != 0) {
                    this.append("&")
                }

                this.append(key).append("=").append(URLEncoder.encode(value.toString(), "utf-8"))
                i++
            }
        }.toByteArray().also { bytes = it }.size.toLong()
    }

    override fun writeData(os: OutputStream) {
        os.write(bytes)
    }
}

@OptIn(ExperimentalTypeInference::class)
public fun FormUrlEncodedBody(@BuilderInference builderAction: MutableMap<String, Any>.() -> Unit): FormUrlEncodedBody {
    return FormUrlEncodedBody(data = buildMap(builderAction))
}
