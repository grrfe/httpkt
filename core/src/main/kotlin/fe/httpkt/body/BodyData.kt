package fe.httpkt.body

import java.io.OutputStream

/**
 * An abstract class which represents the data sent to the server in non-get requests
 * @param name the type of body data this is (e.g. application/json)
 */
public abstract class BodyData(public var name: String?){
    /**
     * This method is called by [fe.httpkt.Request] right before sending the body data.
     * Please keep in mind that the data "processed" by this method should be stored in an internal object somewhere
     * in order for it not having to be re-computed in [writeData].
     *
     * @return the length of the content sent to the server (size of the byte array)
     */
    public abstract fun processContent(): Long

    /**
     * Write the computed data to the output stream of the remote server connection
     */
    public abstract fun writeData(os: OutputStream)
}
