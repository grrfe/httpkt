package fe.httpkt.body

import fe.httpkt.data.HEADER_CONTENT_DISPOSITION
import fe.httpkt.data.HEADER_CONTENT_TYPE
import java.io.File
import java.io.OutputStream
import java.io.PrintWriter
import java.net.URLConnection


public typealias UploadCallback = (String, Long, Long) -> Unit

/**
 * An implementation of [BodyData] allowing the user to send multipart/form-data to the server
 * @param fields a [Map] containing all fields that should be sent to the server
 * @param files a [Map] containing all files that should be sent to the server
 * @param uploadCallback a [UploadCallback] which is invoked everytime [DEFAULT_BUFFER_SIZE] bytes of a file have been uploaded to the server
 */
public class MultipartBody(
    private val fields: Map<String, Any> = emptyMap(),
    private val files: Map<String, File> = emptyMap(),
    private val uploadCallback: UploadCallback? = null
) : BodyData("multipart/form-data") {

    private val boundary = "===" + System.currentTimeMillis() + "==="

    init {
        this.name = "$name; boundary=$boundary"
    }

    public companion object {
        private const val LINE_FEED = "\r\n"
    }

    override fun processContent(): Long = -1L

    private fun PrintWriter.appendLine(str: String, flush: Boolean = false) {
        this.append(str)
        this.newLine(flush)
    }

    private fun PrintWriter.newLine(flush: Boolean = false) {
        this.append(LINE_FEED)
        if (flush) {
            this.flush()
        }
    }

    private fun File.upload(
        out: OutputStream,
        callback: FieldCallback? = null,
        bufferSize: Int = DEFAULT_BUFFER_SIZE
    ) {
        val fileLength = this.length()

        this.inputStream().use {
            var bytesCopied: Long = 0
            val buffer = ByteArray(bufferSize)

            var bytes = it.read(buffer)
            while (bytes >= 0) {
                out.write(buffer, 0, bytes)
                bytesCopied += bytes
                bytes = it.read(buffer)

                callback?.updateValue(bytesCopied, fileLength)
            }
        }
    }

    public data class FieldCallback(private val formField: String, private val callback: UploadCallback) {
        public fun updateValue(uploaded: Long, all: Long) {
            callback.invoke(formField, uploaded, all)
        }
    }

    override fun writeData(os: OutputStream) {
        PrintWriter(os.writer()).use { pw ->
            fields.forEach { (field, value) ->
                pw.appendLine("--$boundary")
                pw.appendLine("$HEADER_CONTENT_DISPOSITION: form-data; name=\"$field\"")
                pw.appendLine("$HEADER_CONTENT_TYPE: text/plain; charset=UTF-8")
                pw.newLine()
                pw.appendLine(value.toString(), flush = true)
            }

            files.forEach { (name, file) ->
                val mimeType = URLConnection.guessContentTypeFromName(file.name) ?: "application/octet-stream"
                val callback = if (uploadCallback != null) FieldCallback(name, uploadCallback) else null

                pw.appendLine("--$boundary")
                pw.appendLine("$HEADER_CONTENT_DISPOSITION: form-data; name=\"$name\"; filename=\"${file.name}\"")
                pw.appendLine("$HEADER_CONTENT_TYPE: $mimeType")
                pw.newLine(flush = true)

                file.upload(os, callback)

                os.flush()
                pw.flush()
            }

            pw.append(LINE_FEED).flush()
            pw.appendLine("--$boundary--")
        }
    }
}
