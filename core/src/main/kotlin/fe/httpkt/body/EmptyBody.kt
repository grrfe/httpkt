package fe.httpkt.body

import java.io.OutputStream

/**
 * This class should be used when no body should be sent to the server.
 */
public class EmptyBody : BodyData(null){
    override fun processContent(): Long = 0

    override fun writeData(os: OutputStream) {
    }
}
