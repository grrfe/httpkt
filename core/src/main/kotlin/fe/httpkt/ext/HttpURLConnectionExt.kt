package fe.httpkt.ext

import fe.httpkt.ConnectionToType
import fe.httpkt.CustomSuccess
import fe.httpkt.HttpResult
import fe.httpkt.data.Cookie
import fe.httpkt.data.Header
import fe.httpkt.data.toDomain
import fe.httpkt.stream.isGzipped
import fe.httpkt.util.*
import java.io.BufferedReader
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.nio.charset.Charset
import java.util.zip.GZIPInputStream

public typealias WriteStatus = (bytesWritten: Long, byteLength: Long) -> Unit?

/**
 * Determines if the [InputStream] from [HttpURLConnection] should be wrapped with a [GZIPInputStream] or not
 *
 * @param streamType determines which [StreamType] should be used (default = [StreamType.Auto])
 * @return the [InputStream] which has either been wrapped using [GZIPInputStream] or not
 */
public fun HttpURLConnection.getGZIPOrDefaultStream(streamType: StreamType = StreamType.Auto): InputStream {
    return streamType.retrieve(this).let { stream ->
        if (isGzipped) GZIPInputStream(stream)
        else stream
    }
}

/**
 * Writes all data from [HttpURLConnection] to the given [File]
 *
 * @param file The file to write to
 * @param streamType The stream type to use to read from the [HttpURLConnection], default = [StreamType.Auto]
 * @param statusUpdate A handler which gets called everytime [bufferSize] bytes have been written to disk, default = null
 * @param append Append to existing [file], default = false
 * @param bufferSize The size of the buffer to use, default = [DEFAULT_BUFFER_SIZE]
 **/
public fun HttpURLConnection.writeToFile(
    file: File,
    streamType: StreamType = StreamType.Auto,
    statusUpdate: WriteStatus? = null,
    append: Boolean = false,
    bufferSize: Int = DEFAULT_BUFFER_SIZE
): File {
    getGZIPOrDefaultStream(streamType).use { input ->
        val length = this.contentLengthLong
        val updateHandler: CopyStatus? = if (statusUpdate != null) { copiedOverall ->
            statusUpdate.invoke(copiedOverall, length)
        } else null

        FileOutputStream(file, append).use { fos ->
            copyTo(input, fos, bufferSize, updateHandler)
        }
    }

    return file
}

public typealias OutputFileBuilder = (name: String?, ext: String?) -> File

/**
 * Writes all data from [HttpURLConnection] to a [File] constructed from a filename at the end of the string (e.g. /dl/xyz.zip)
 *
 * @param file The file to write to
 * @param streamType The stream type to use to read from the [HttpURLConnection], default = [StreamType.Auto]
 * @param statusUpdate A handler which gets called everytime [bufferSize] bytes have been written to disk, default = null
 * @param append Append to existing [file], default = false
 * @param bufferSize The size of the buffer to use, default = [DEFAULT_BUFFER_SIZE]
 **/
public fun HttpURLConnection.writeToFile(
    outputFileBuilder: OutputFileBuilder,
    streamType: StreamType = StreamType.Auto,
    statusUpdate: WriteStatus? = null,
    append: Boolean = false,
    bufferSize: Int = DEFAULT_BUFFER_SIZE
): File {
    val (fileName, ext) = Extension.getFileNameFromUrl(url)
    return writeToFile(outputFileBuilder(fileName, ext), streamType, statusUpdate, append, bufferSize)
}


/**
 * Read all text from [HttpURLConnection]
 *
 * @param streamType The stream type to use to read from the [HttpURLConnection], default = [StreamType.Auto]
 * @return the text as [String]
 */
public fun HttpURLConnection.readToString(
    streamType: StreamType = StreamType.Auto,
    charset: Charset = Charsets.UTF_8
) = bufferedReader(streamType, charset).use { it.readText() }


/**
 * Read all text from [HttpURLConnection] and split it by lines
 *
 * @param streamType The stream type to use to read from the [HttpURLConnection], default = [StreamType.Auto]
 * @return a [List] containing all lines
 */
public fun HttpURLConnection.readToLines(
    streamType: StreamType = StreamType.Auto,
    charset: Charset = Charsets.UTF_8
) = bufferedReader(streamType, charset).use { it.readLines() }


/**
 * Create a [BufferedReader] on [HttpURLConnection]
 *
 * @param streamType The stream type to use [HttpURLConnection], default = [StreamType.Auto]
 * @return a [List] containing all lines
 */
public fun HttpURLConnection.bufferedReader(
    streamType: StreamType = StreamType.Auto,
    charset: Charset = Charsets.UTF_8
) = getGZIPOrDefaultStream(streamType).bufferedReader(charset)

public fun <T> HttpURLConnection.setOnCustomSuccess(
    customSuccess: CustomSuccess? = null,
    handler: ConnectionToType<T>
): HttpResult<T> {
    return if (this.setOnCustomSuccess(customSuccess).second) HttpResult.HttpSuccess(handler(this)) else HttpResult.HttpFailure(
        this
    )
}

public fun HttpURLConnection.setOnCustomSuccess(customSuccess: CustomSuccess? = null): Pair<HttpURLConnection, Boolean> {
    return this to (customSuccess?.invoke(this) ?: this.isHttpSuccess())
}

public fun HttpURLConnection.isHttpSuccess() = fe.httpkt.isHttpSuccess(this.responseCode)
public fun HttpURLConnection.isHttpRedirect() = fe.httpkt.isHttpRedirect(this.responseCode)

public fun HttpURLConnection.locationHeader() = this.findHeader("Location")

public fun HttpURLConnection.getUrlLocation(): String? {
    this.locationHeader()?.firstOrNull()?.let { location ->
        return URL(this.url, location).toString()
    }

    return null
}

/**
 * Print all headers of the [HttpURLConnection]
 */
public fun HttpURLConnection.debugHeaders() {
    this.headerFields.forEach { (k, v) ->
        println("$k - $v")
    }
}

/**
 * Get all headers from [HttpURLConnection]
 *
 * @return a [List] containing all [Header]s
 */
public fun HttpURLConnection.headers() = headerFields.map { (name, values) ->
    Header(name, values)
}

/**
 * Get all cookies from [HttpURLConnection]
 *
 * @return a [List] containing all [Cookie]s
 */
public fun HttpURLConnection.cookies(): List<Cookie> {
    return findHeader(Extension.setCookieHeader)?.values?.map { Cookie.create(it, url.toString()) } ?: emptyList()
}

/**
 * Finds a specific header from [HttpURLConnection]
 *
 * @param name the name of the header to search for
 * @return the requested [Header] or null if not found
 */
public fun HttpURLConnection.findHeader(name: String): Header? {
    headerFields.keys.forEach { key ->
        if (name.equals(key, ignoreCase = true)) {
            return headerFields[key]?.let { Header(key, it) }
        }
    }

    return null
}

/**
 * Find a specific cookie from [HttpURLConnection]
 *
 * @param name the name of the cookie to search for
 * @return the requested [Cookie] or null if not found
 */
public fun HttpURLConnection.findCookie(name: String): Cookie? {
    return cookies().find { it.name == name }
}

/**
 * Get the domain of the url requested by [HttpURLConnection]
 *
 * @return the domain
 */
public fun HttpURLConnection.getDomain() = url.toString().toDomain()
