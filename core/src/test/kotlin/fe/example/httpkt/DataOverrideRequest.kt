package fe.example.httpkt

import fe.httpkt.Request
import fe.httpkt.data.Header
import fe.httpkt.ext.readToString

fun main() {
    //Create Request instance with custom header (User-Agent)
    val request = Request(data = {
        this.addHeader(Header("User-Agent", "httpkt/13"))
    })

    //Send get request without overriding header
    val conNoOverride = request.get("https://httpbin.org/get")

    //User-Agent is "httpkt/13"
    println(conNoOverride.readToString())

    val conOverride = request.getFn("https://httpbin.org/get", dataBuilder = {
        //set custom data for this specific request, override header of Request instance
        this.addHeader(Header("User-Agent", "nothttpkt"))
    })

    //User-Agent is "nothttpkt"
    println(conOverride.readToString())
}
