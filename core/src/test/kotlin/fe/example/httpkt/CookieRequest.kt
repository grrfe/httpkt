package fe.example.httpkt

import fe.httpkt.Request
import fe.httpkt.data.Cookie
import fe.httpkt.ext.readToString

fun main() {
    //Create  an instance of Request with data (a cookie, in this case)
    val request = Request(data = {
        //use Cookie.of to build a cookie
        val cookie = Cookie.of("remember", "me")

        //add cookie to this instance
        this.addCookie(cookie)
    })

    val con = request.get("https://httpbin.org/get", )
    println(con.readToString())
}
