package fe.example.httpkt

import fe.httpkt.Request
import fe.httpkt.ext.readToString
import java.net.HttpURLConnection

fun main() {
    //Create an instance of Request with its default parameters
    val request = Request()

    //Send a get request to httpbin.org's get endpoint (which returns all data about the request)
    val con: HttpURLConnection = request.get("https://httpbin.org/get")

    //Print the result of request (readToString() is an extension function, we'll get to that later)
    println(con.readToString())
}
