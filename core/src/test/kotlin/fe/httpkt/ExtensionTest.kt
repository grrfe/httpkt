package fe.httpkt

import fe.httpkt.util.Extension
import kotlin.test.Test
import kotlin.test.assertEquals
import java.net.URL

internal class ExtensionTest {
    @Test
    fun testSplitDomain() {
        assertEquals(true, Extension.splitDomain("google.com") == ("google.com" to emptyList<String>()))
        assertEquals(true, Extension.splitDomain(".google.com") == ("google.com" to emptyList<String>()))
        assertEquals(true, Extension.splitDomain("test.google.com") == ("google.com" to listOf("test")))
        assertEquals(true, Extension.splitDomain("test.test2.google.com") == ("google.com" to listOf("test2", "test")))
        assertEquals(false, Extension.splitDomain("test.test2.google.com") == ("google.com" to listOf("test", "test2")))
        assertEquals(false, Extension.splitDomain("test.google.com") == ("google.com" to emptyList<String>()))
    }

    @Test
    fun testUrlToDomain() {
        assertEquals(true, Extension.urlToDomain("https://google.com") == ("google.com"))
        assertEquals(true, Extension.urlToDomain("https://google.com/test/test2") == ("google.com"))
        assertEquals(true, Extension.urlToDomain("https://test.google.com/test/test2") == ("test.google.com"))
        assertEquals(
            true,
            Extension.urlToDomain("https://test2.test.google.com/test/test2") == ("test2.test.google.com")
        )
        assertEquals(true, Extension.urlToDomain("google.com") == ("google.com"))
    }

    @Test
    fun testGetFileFromUrl() {
        mapOf(
            ("file" to "ext") to "https://test.com/file.ext",
            ("file" to "ext") to "https://test.com/test/test123/file.ext",
            ("file" to "ext") to "https://test.com/test/test123/file.ext?yeet=lol",
            ("file" to "ext") to "https://test.com/test/test123/file.ext?yeet=lol#lel",
            ("file.yeet" to "ext") to "https://test.com/file.yeet.ext",
            ("file.yeet" to "ext") to "https://test.com/xd/file.yeet.ext?hello=world#ye",
            ("file" to null) to "https://test.com/file",
            (null to null) to "https://test.com/",
            (null to null) to "https://test.com",
        ).forEach { (expected, inputUrl) ->
            assertEquals(expected, Extension.getFileNameFromUrl(URL(inputUrl)))
        }
    }
}
