package fe.httpkt

import fe.httpkt.data.Cookie
import fe.httpkt.proxy.AuthProxy
import java.net.InetSocketAddress
import java.net.Proxy
import kotlin.test.Test
import kotlin.test.assertEquals

internal class HttpDataTest {
    @Test
    fun testMerge() {
        val proxy = AuthProxy(Proxy(Proxy.Type.SOCKS, InetSocketAddress("6.9.8.8", 1337)))

        val httpData1 = HttpData.of {
            this.headers {
                "hello"("world")
            }

            this.addHeaderImpl("foo", "bar")
            this.addHeaderImpl("test", "123")
            this.addCookie(Cookie.of("my", "cookie"))
            this.addCookie(Cookie.of("JAWOLL", "franz"))

            this.proxy = AuthProxy(Proxy.NO_PROXY)
        }

        val httpData2 = HttpData.of {
            this.addHeaderImpl("foo", "baz")
            this.removeExistingHeader("test")
            this.removeCookie("JAWOLL")
            this.proxy = proxy
        }

        val resultHttpData = HttpData.of {
            this.addHeaderImpl("hello", "world")
            this.addHeaderImpl("foo", "baz")
            this.addCookie(Cookie.of("my", "cookie"))
            this.proxy = AuthProxy(Proxy.NO_PROXY)
        }

        assertEquals(resultHttpData, httpData1.plus(httpData2))

        val httpData2_1 = HttpData.of {
            this.proxy = proxy
        }

        val httpData2_2 = HttpData.of {
            this.proxy = AuthProxy()
            this.proxyState = HttpData.Builder.ProxyState.Replace
        }

        assertEquals(HttpData.of { this.proxy = AuthProxy() }, httpData2_1 + httpData2_2)

        val httpData3_1 = HttpData.of {
            this.proxy = proxy
        }

        val httpData3_2 = HttpData.of {
            this.proxyState = HttpData.Builder.ProxyState.Remove
        }



        assertEquals(HttpData.of {}, httpData3_1 + httpData3_2)
    }
}

