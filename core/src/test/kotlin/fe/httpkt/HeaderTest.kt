package fe.httpkt

import kotlin.test.Test

internal class HeaderTest {
    @Test
    fun headerTest(){
        HttpData.of {
            this.headers {
                "hello"("world", "Ttest")
            }
        }
    }
}
