package fe.httpkt

import fe.httpkt.ext.readToString
import fe.httpkt.ext.setOnCustomSuccess
import kotlin.test.Test

internal class HttpSuccessTest {
    @Test
    fun testHttpSuccessResult() {
        val request = Request()
        val (con, success) = request.get("https://httpbin.org/get")
            .setOnCustomSuccess { con -> con.responseCode == 1337 }
        if (success) {
            println(con.readToString())
        }

        val httpResult =
            request.get("https://httpbin.org/get").setOnCustomSuccess(null) { handler -> handler.readToString() }
        if (httpResult is HttpResult.HttpSuccess) {
            httpResult.value //String
        } else if (httpResult is HttpResult.HttpFailure) {
            httpResult.con // url connection
        }
    }
}
