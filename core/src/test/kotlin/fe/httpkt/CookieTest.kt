package fe.httpkt

import fe.httpkt.data.Cookie
import fe.httpkt.data.toDomain
import org.junit.runners.model.MultipleFailureException
import java.time.LocalDateTime
import kotlin.test.Test
import kotlin.test.assertEquals

internal class CookieTest {

    @Test
    fun testCreate() {
        val noExpires: (Cookie) -> Unit = { cookie -> assertEquals(null, cookie.expires) }
        val noMaxAge: (Cookie) -> Unit = { cookie -> assertEquals(-1, cookie.maxAge) }
        val noDomain: (Cookie) -> Unit = { cookie -> assertEquals(null, cookie.domain) }
        val noPath: (Cookie) -> Unit = { cookie -> assertEquals("/", cookie.path) }

        fun assertName(expected: String): (Cookie) -> Unit = { cookie -> assertEquals(expected, cookie.name) }
        fun assertValue(expected: String): (Cookie) -> Unit = { cookie -> assertEquals(expected, cookie.value) }
        fun assertExpires(expected: LocalDateTime): (Cookie) -> Unit =
            { cookie -> assertEquals(expected, cookie.expires) }

        fun assertMaxAge(expected: Int): (Cookie) -> Unit = { cookie -> assertEquals(expected, cookie.maxAge) }
        fun assertDomain(expected: String): (Cookie) -> Unit = { cookie -> assertEquals(expected, cookie.domain?.mainDomain) }
        fun assertPath(expected: String): (Cookie) -> Unit = { cookie -> assertEquals(expected, cookie.path) }

        mapOf(
            "Set-Cookie: sessionId=38afes7a8" to listOf(
                assertName("sessionId"), assertValue("38afes7a8"), noExpires, noMaxAge, noDomain, noPath
            ),
            "Set-Cookie: id=a3fWa; Expires=Wed, 21 Oct 2015 07:28:00 GMT" to listOf(
                assertName("id"),
                assertValue("a3fWa"),
                assertExpires(LocalDateTime.of(2015, 10, 21, 7, 28)),
                noMaxAge, noDomain, noPath
            ),
            "Set-Cookie: id=a3fWa; Max-Age=2592000" to listOf(
                assertName("id"), assertValue("a3fWa"), noExpires, assertMaxAge(2592000), noDomain, noPath
            ),
            "Set-Cookie: 2X_AAR=2021-07-11-11; expires=Tue, 18-Sep-2021 15:44:01 GMT; path=/; domain=.google.com; Secure; SameSite=none" to listOf(
                assertName("2X_AAR"), assertValue("2021-07-11-11"),
                assertExpires(LocalDateTime.of(2021, 9, 18, 15, 44, 1)),
                noMaxAge, assertDomain("google.com"), noPath
            ),
            "set-cookie: CGIC=fXpsD247tTLBDYS8zzBqNqc4U2NUPmgj; expires=Fri, 08-Feb-2024 11:18:24 GMT; path=/complete/search; domain=.google.com; HttpOnly" to listOf(
                assertName("CGIC"), assertValue("fXpsD247tTLBDYS8zzBqNqc4U2NUPmgj"),
                assertExpires(LocalDateTime.of(2024, 2, 8, 11, 18, 24)),
                noMaxAge, assertDomain("google.com"), assertPath("/complete/search")
            ),
            "hello=world" to listOf(assertName("hello"), assertValue("world")),
            "yeet=\"lol=dsafdsafads\"" to listOf(assertName("yeet"), assertValue("\"lol=dsafdsafads\"")),
            "Set-Cookie: HAans=" to listOf(assertName("HAans"), assertValue(""))
        ).map { (cookieHeader, checks) ->
            val cookie = Cookie.create(cookieHeader)
            val failures: MutableList<Throwable> = ArrayList()
            checks.forEach { executable ->
                kotlin.runCatching {
                    executable.invoke(cookie)
                }.getOrElse {
                    failures.add(it)
                }
            }

            if (failures.isNotEmpty()) {
                throw MultipleFailureException(failures)
            }
        }
    }

    @Test
    fun testMatchesDomain() {
        val cookie = Cookie.Builder(name = "hello", value = "world", domain = "google.com".toDomain()).build()
        assertEquals(true, cookie.matches("google.com"))
        assertEquals(true, cookie.matches("https://google.com"))
        assertEquals(true, cookie.matches("https://google.com/foo"))
        assertEquals(true, cookie.matches("https://google.com/blah/blah/blah"))
        assertEquals(true, cookie.matches("hi.google.com"))
        assertEquals(true, cookie.matches("www.yoink.google.com/owo"))
        assertEquals(false, cookie.matches("goggle.com"))

        val cookie2 = Cookie.Builder(name = "foo", value = "bar").build()
        assertEquals(true, cookie2.matches(""))
        assertEquals(true, cookie2.matches("https://dsahjkfdhjslfjkwhziu32.com"))

        val cookie3 = Cookie.Builder(name = "foo", value = "bar", domain = "sub-domain.main-domain.com".toDomain()).build()
        assertEquals(true, cookie3.matches("https://sub-domain.main-domain.com"))
        assertEquals(false, cookie3.matches("https://main-domain.com"))

        val cookie4 = Cookie.Builder(name = "foo", value = "bar", domain = ".main-domain.com".toDomain()).build()
        assertEquals(true, cookie4.matches("https://sub-domain.main-domain.com"))
        assertEquals(true, cookie4.matches("https://main-domain.com"))
    }

    @Test
    fun testHasExpired() {
        val cookie = Cookie.Builder(name = "hello", value = "world", domain = "google.com".toDomain()).build()
        assertEquals(false, cookie.hasExpired())

        val cookie2 = Cookie.Builder(name = "foo", value = "bar", maxAge = 10).build()
        assertEquals(false, cookie2.hasExpired())
        assertEquals(true, cookie2.hasExpired(LocalDateTime.now().plusDays(1)))

        val cookie3 = Cookie.Builder(name = "foo", value = "bar", expires = LocalDateTime.now().plusSeconds(30)).build()
        assertEquals(true, cookie3.hasExpired(LocalDateTime.now().plusDays(1)))
        assertEquals(false, cookie3.hasExpired(LocalDateTime.now().minusDays(1)))
    }
}
