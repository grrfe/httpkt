package fe.httpkt

import fe.httpkt.data.toDomain
import kotlin.test.Test
import kotlin.test.assertEquals

internal class DomainTest {
    @Test
    fun testIsCookieDomain() {
        assertEquals(true, "google.com".toDomain().isCookieDomain("https://google.com/search"))
        assertEquals(true, ".google.com".toDomain().isCookieDomain("https://google.com/search"))
        assertEquals(true, "google.com".toDomain().isCookieDomain("https://test.google.com/test2"))
        assertEquals(true, "test.google.com".toDomain().isCookieDomain("https://test.google.com/test2"))
        assertEquals(true, "test.google.com".toDomain().isCookieDomain("https://test2.test.google.com/test2"))
        assertEquals(false, "test.test2.google.com".toDomain().isCookieDomain("https://test.google.com/test2"))
        assertEquals(true, "test.test.google.com".toDomain().isCookieDomain("https://test.test.google.com/test2"))
    }
}
