package fe.httpkt

import fe.httpkt.ext.readToString
import kotlin.test.Test

internal class CookieSendTest {
    @Test
    fun sendCookies() {
        val req = Request(HttpData.of {
            addCookie("hello", "world")
            addCookie("foo", "bar")
        })

        println(
            req.get("https://httpbin.org/get").readToString()
        )
    }
}
