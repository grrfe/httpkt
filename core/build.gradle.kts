plugins {
}

dependencies{
    implementation(KotlinX.coroutines.core)

    testImplementation(kotlin("test"))
    testImplementation("com.willowtreeapps.assertk:assertk:_")
}
