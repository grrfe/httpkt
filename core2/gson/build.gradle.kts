plugins {
}

dependencies {
    api(project(":core"))
    api(project(":serialization-gson"))
    api(project(":core2-core"))
    api("com.google.code.gson:gson:_")

    testImplementation(kotlin("test"))
}
