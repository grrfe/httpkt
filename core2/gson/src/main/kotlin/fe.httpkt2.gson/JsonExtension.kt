package fe.httpkt2.gson

import com.google.gson.Gson
import com.google.gson.JsonElement
import fe.httpkt.json.JsonContext
import fe.httpkt.json.asType
import fe.httpkt.json.fromJson
import fe.httpkt.json.readToJson
import fe.httpkt.util.StreamType
import fe.httpkt2.Either
import fe.httpkt2.HttpResponse
import fe.httpkt2.asEither
import java.lang.reflect.Type

public inline fun <reified T : JsonElement> HttpResponse.readToJsonElementEither(
    streamType: StreamType = StreamType.Auto,
): Either<Throwable, T> {
    return runCatching { connection.readToJson(streamType) as T }.asEither()
}

public inline fun <reified T> HttpResponse.fromJsonToEither(
    streamType: StreamType = StreamType.Auto,
    gson: Gson = JsonContext.default,
    type: Type = asType<T>(),
): Either<Throwable, T> {
    return runCatching { connection.fromJson<T>(streamType, gson, type) }.asEither()
}
