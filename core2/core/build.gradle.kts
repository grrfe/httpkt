plugins {
}

dependencies{
    api(project(":core"))

    implementation(KotlinX.coroutines.core)
    testImplementation(KotlinX.coroutines.test)

    testImplementation(kotlin("test"))
    testImplementation("com.willowtreeapps.assertk:assertk:_")
}
