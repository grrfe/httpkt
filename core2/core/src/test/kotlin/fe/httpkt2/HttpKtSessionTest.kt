package fe.httpkt2

import fe.httpkt.Session
import fe.httpkt.ext.debugHeaders
import fe.httpkt.ext.headers
import fe.httpkt.ext.readToString
import kotlinx.coroutines.test.runTest
import kotlin.test.Test

internal class HttpKtSessionTest {
    @Test
    fun test() = runTest {
        val session = HttpKtSession()
        val result = session.get(url = Url("https://httpbin.org/cookies/set/hello/world"))
        val response = result as HttpResponse
        response.connection.debugHeaders()

        println(response.connection.readToString())

        val result2 = session.get(url = Url("https://httpbin.org/cookies"))
        val response2 = result2 as HttpResponse
        println(response2.connection.readToString())
    }

    @Test
    fun test2()  {
        val session = Session()
        val connection = session.get(url = "https://httpbin.org/cookies/set/hello/world", forceSend = true)
        connection.debugHeaders()

        println(connection.readToString())

        val connection2 = session.get(url = "https://httpbin.org/cookies")
        println(connection2.readToString())
    }
}
