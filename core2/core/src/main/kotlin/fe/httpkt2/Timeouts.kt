package fe.httpkt2

public data class Timeouts(val connect: Int, val read: Int) {
    public companion object {
        public val None: Timeouts = Timeouts(0, 0)
    }
}
