package fe.httpkt2

import fe.httpkt.ext.isHttpSuccess
import java.net.HttpURLConnection

public sealed interface HttpResult2

public class HttpResponse(public val connection: HttpURLConnection) : HttpResult2 {
    public val isSuccess: Boolean by lazy { connection.isHttpSuccess() }
}

public class HttpFailure(public val throwable: Throwable) : HttpResult2
