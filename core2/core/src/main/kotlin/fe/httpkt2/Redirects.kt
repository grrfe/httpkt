package fe.httpkt2

public data class Redirects(val follow: Boolean, val max: Int) {
    public companion object {
        public val Default: Redirects = Redirects(true, 5)
    }
}
