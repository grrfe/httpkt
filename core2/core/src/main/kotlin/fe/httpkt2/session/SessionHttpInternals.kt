package fe.httpkt2.session

import fe.httpkt.HttpData
import fe.httpkt.debuggingEnabled
import fe.httpkt.ext.cookies
import fe.httpkt.internal.HttpInternals
import fe.httpkt2.DataStore
import java.net.HttpURLConnection

public class SessionHttpInternals(
    public val delegate: HttpInternals,
    private val store: DataStore,
) : HttpInternals by delegate {
    override fun handleRedirections(
        con: HttpURLConnection,
        followRedirects: Boolean,
        maxRedirects: Int,
        data: HttpData,
    ): HttpURLConnection {
        if (debuggingEnabled) println("Handling redirections for ${con.url}")

        var connection: HttpURLConnection = con
        var redirected: Boolean

        var counter = 0
        do {
            with(followRedirect(connection, data)) {
                //The cookie collector in Session.kt is implemented in this method, so we just call it ALWAYS,
                // but then immediately return the original connection
                if (!followRedirects) {
                    return con
                }

                if (debuggingEnabled) println("Followed redirect, new http con = $this")

                if (this != null) {
                    connection = this
                }

                redirected = this != null
            }

            if (counter == maxRedirects) {
                return connection
            }

            if (redirected) {
                counter++
            }
        } while (redirected)

        return connection
    }

    override fun followRedirect(con: HttpURLConnection, data: HttpData): HttpURLConnection? {
        val cookies = con.cookies()
        println("FOLLOWING redirect: ${cookies.size}")

        store.addCookies(cookies.also {
            if (debuggingEnabled) println("Collected ${it} for ${con.url}")
        })

        return delegate.followRedirect(con, store.mergeData(data))
    }
}
