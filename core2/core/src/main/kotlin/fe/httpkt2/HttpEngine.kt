package fe.httpkt2

import fe.httpkt.HttpData
import fe.httpkt.body.BodyData
import fe.httpkt.body.EmptyBody
import fe.httpkt.data.HttpHeaders
import fe.httpkt.internal.HttpInternals
import fe.httpkt.internal.forceSend
import fe.httpkt.reflection.GlobalReflectionCoordinator
import fe.httpkt.reflection.ReflectionCoordinator
import kotlinx.coroutines.*
import java.net.HttpURLConnection
import java.net.URL
import kotlin.coroutines.CoroutineContext

public class HttpEngine(
    private val internals: HttpInternals,
    private val reflectionCoordinator: ReflectionCoordinator = GlobalReflectionCoordinator,
    private val dispatcher: CoroutineContext = Dispatchers.IO,
) : CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = dispatcher + SupervisorJob() + CoroutineName("HttpEngine")

    private suspend fun <R> tryCatch(block: suspend () -> R): Result<R> {
        return runCatching {
            async { block() }.await()
        }
    }

    public suspend fun setupConnection(
        method: BaseHttpMethod,
        url: Url,
        data: HttpData,
        timeouts: Timeouts = Timeouts.None,
        redirects: Redirects = Redirects.Default,
    ): HttpURLConnection = async {
        ensureActive()

        val proxy = AuthProxy2Compat.toAuthProxy2(data.proxy)
        val url = URL(url.toString())

        ensureActive()
        val connection = proxy.openConnection(url) as HttpURLConnection

        connection.requestMethod = method.verb
        // force to false so we can handle it manually
        connection.instanceFollowRedirects = false
        connection.connectTimeout = timeouts.connect
        connection.readTimeout = timeouts.read

        ensureActive()
        connection
    }.await()

    public suspend fun noBodyRequest(
        method: HttpNoBodyMethod,
        url: Url,
        data: HttpData,
        timeouts: Timeouts = Timeouts.None,
        redirects: Redirects = Redirects.Default,
        forceSend: Boolean = false,
    ): HttpResult2 = tryCatch {
        ensureActive()

        ensureActive()
        val connection = setupConnection(method, url, data, timeouts, redirects)
        internals.applyRequestProperties(connection, data)
        internals.handleRedirections(connection, redirects.follow, redirects.max, data)

        ensureActive()
        connection.forceSend(forceSend)
        HttpResponse(connection)
    }.getOrElse { HttpFailure(it) }

    public suspend fun request(
        method: HttpMethod,
        url: Url,
        data: HttpData,
        body: BodyData = EmptyBody(),
        timeouts: Timeouts = Timeouts.None,
        redirects: Redirects = Redirects.Default,
        forceSend: Boolean = false,
    ): HttpResult2 = tryCatch {
        ensureActive()
        if (method == HttpMethod.Patch) {
            reflectionCoordinator.tryPatchHttpMethods()
        }

        ensureActive()
        val connection = setupConnection(method, url, data, timeouts, redirects)
        connection.doOutput = true

        ensureActive()
        val emptyBody = (body is EmptyBody)
        if (!emptyBody) {
            connection.setRequestProperty(HttpHeaders.ContentType, body.name)
        }

        ensureActive()
        val bodySize = body.processContent()
        if (bodySize != -1L) {
            connection.setFixedLengthStreamingMode(bodySize)
        }

        ensureActive()
        internals.applyRequestProperties(connection, data)
        connection.useCaches = false

        ensureActive()
        connection.connect()

        ensureActive()
        if (!emptyBody) {
            body.writeData(connection.outputStream)
        }

        ensureActive()
        internals.handleRedirections(connection, redirects.follow, redirects.max, data)
        connection.forceSend(forceSend)

        HttpResponse(connection)
    }.getOrElse { HttpFailure(it) }
}
