package fe.httpkt2

import fe.httpkt.HttpData
import fe.httpkt.HttpDataBuilder
import fe.httpkt.body.BodyData
import fe.httpkt.body.EmptyBody
import fe.httpkt.data.Cookie
import fe.httpkt.internal.HttpInternals
import fe.httpkt.internal.RealHttpInternals
import fe.httpkt2.session.SessionHttpInternals

public interface DataStore {
    public fun addCookies(cookies: List<Cookie>)
    public fun mergeData(data: HttpData?): HttpData
}

public class SessionDataStore(
    public val instanceData: HttpData,
    public val sessionCookies: HttpData.Builder
) : DataStore {
    override fun addCookies(cookies: List<Cookie>) {
        for (cookie in cookies) {
            sessionCookies.addCookie(cookie)
        }
    }

    override fun mergeData(data: HttpData?): HttpData {
        val merged = this.instanceData.merge(this.sessionCookies.build(), HttpData.MergeType.Cookies)
        return with(merged) {
            if (data != null) this + data else this
        }
    }
}

public open class HttpKtSession(
    private var instanceData: HttpData = HttpData.default,
    private val sessionCookies: HttpData.Builder = HttpData.Builder(),
    private val store: DataStore = SessionDataStore(instanceData, sessionCookies),
    private val internals: HttpInternals = SessionHttpInternals(RealHttpInternals(), store),
    private val engine: HttpEngine = HttpEngine(internals),
) {
    public fun update(data: HttpData): HttpData {
        this.instanceData += data
        this.sessionCookies.mergeCookies(data)

        return this.instanceData
    }

    public fun update(builder: HttpDataBuilder): HttpData {
        return update(HttpData.of(builder))
    }

    public suspend fun head(
        url: Url,
        data: HttpData? = null,
        timeouts: Timeouts = Timeouts.None,
        redirects: Redirects = Redirects.Default,
        forceSend: Boolean = false,
    ): HttpResult2 {
        return engine.noBodyRequest(HttpNoBodyMethod.Head, url, store.mergeData(data), timeouts, redirects, forceSend)
    }

    public open suspend fun get(
        url: Url,
        data: HttpData? = null,
        timeouts: Timeouts = Timeouts.None,
        redirects: Redirects = Redirects.Default,
        forceSend: Boolean = false,
    ): HttpResult2 {
        return engine.noBodyRequest(HttpNoBodyMethod.Get, url, store.mergeData(data), timeouts, redirects, forceSend)
    }

    public open suspend fun post(
        url: Url,
        data: HttpData? = null,
        body: BodyData = EmptyBody(),
        timeouts: Timeouts = Timeouts.None,
        redirects: Redirects = Redirects.Default,
        forceSend: Boolean = false,
    ): HttpResult2 {
        return engine.request(HttpMethod.Post, url, store.mergeData(data), body, timeouts, redirects, forceSend)
    }

    public open suspend fun put(
        url: Url,
        data: HttpData? = null,
        body: BodyData = EmptyBody(),
        timeouts: Timeouts = Timeouts.None,
        redirects: Redirects = Redirects.Default,
        forceSend: Boolean = false,
    ): HttpResult2 {
        return engine.request(HttpMethod.Put, url, store.mergeData(data), body, timeouts, redirects, forceSend)
    }

    public open suspend fun patch(
        url: Url,
        data: HttpData? = null,
        body: BodyData = EmptyBody(),
        timeouts: Timeouts = Timeouts.None,
        redirects: Redirects = Redirects.Default,
        forceSend: Boolean = false,
    ): HttpResult2 {
        return engine.request(HttpMethod.Patch, url, store.mergeData(data), body, timeouts, redirects, forceSend)
    }

    public open suspend fun delete(
        url: Url,
        data: HttpData? = null,
        body: BodyData = EmptyBody(),
        timeouts: Timeouts = Timeouts.None,
        redirects: Redirects = Redirects.Default,
        forceSend: Boolean = false,
    ): HttpResult2 {
        return engine.request(HttpMethod.Delete, url, store.mergeData(data), body, timeouts, redirects, forceSend)
    }

    public open suspend fun options(
        url: Url,
        data: HttpData? = null,
        body: BodyData = EmptyBody(),
        timeouts: Timeouts = Timeouts.None,
        redirects: Redirects = Redirects.Default,
        forceSend: Boolean = false,
    ): HttpResult2 {
        return engine.request(HttpMethod.Options, url, store.mergeData(data), body, timeouts, redirects, forceSend)
    }
}
