package fe.httpkt2

import fe.httpkt.HttpData
import fe.httpkt.body.BodyData
import fe.httpkt.body.EmptyBody
import fe.httpkt.internal.HttpInternals
import fe.httpkt.internal.RealHttpInternals

public open class HttpKtRequest(
    private val internals: HttpInternals = RealHttpInternals(),
    private var instanceData: HttpData = HttpData.default,
    private val engine: HttpEngine = HttpEngine(internals),
) {
    private fun mergeData(data: HttpData?): HttpData {
        return if (data != null) this.instanceData + data else this.instanceData
    }

    public suspend fun head(
        url: Url,
        data: HttpData? = null,
        timeouts: Timeouts = Timeouts.None,
        redirects: Redirects = Redirects.Default,
        forceSend: Boolean = false,
    ): HttpResult2 {
        return engine.noBodyRequest(HttpNoBodyMethod.Head, url, mergeData(data), timeouts, redirects, forceSend)
    }

    public open suspend fun get(
        url: Url,
        data: HttpData? = null,
        timeouts: Timeouts = Timeouts.None,
        redirects: Redirects = Redirects.Default,
        forceSend: Boolean = false,
    ): HttpResult2 {
        return engine.noBodyRequest(HttpNoBodyMethod.Get, url, mergeData(data), timeouts, redirects, forceSend)
    }

    public open suspend fun post(
        url: Url,
        data: HttpData? = null,
        body: BodyData = EmptyBody(),
        timeouts: Timeouts = Timeouts.None,
        redirects: Redirects = Redirects.Default,
        forceSend: Boolean = false,
    ): HttpResult2 {
        return engine.request(HttpMethod.Post, url, mergeData(data), body, timeouts, redirects, forceSend)
    }

    public open suspend fun put(
        url: Url,
        data: HttpData? = null,
        body: BodyData = EmptyBody(),
        timeouts: Timeouts = Timeouts.None,
        redirects: Redirects = Redirects.Default,
        forceSend: Boolean = false,
    ): HttpResult2 {
        return engine.request(HttpMethod.Put, url, mergeData(data), body, timeouts, redirects, forceSend)
    }

    public open suspend fun patch(
        url: Url,
        data: HttpData? = null,
        body: BodyData = EmptyBody(),
        timeouts: Timeouts = Timeouts.None,
        redirects: Redirects = Redirects.Default,
        forceSend: Boolean = false,
    ): HttpResult2 {
        return engine.request(HttpMethod.Patch, url, mergeData(data), body, timeouts, redirects, forceSend)
    }

    public open suspend fun delete(
        url: Url,
        data: HttpData? = null,
        body: BodyData = EmptyBody(),
        timeouts: Timeouts = Timeouts.None,
        redirects: Redirects = Redirects.Default,
        forceSend: Boolean = false,
    ): HttpResult2 {
        return engine.request(HttpMethod.Delete, url, mergeData(data), body, timeouts, redirects, forceSend)
    }

    public open suspend fun options(
        url: Url,
        data: HttpData? = null,
        body: BodyData = EmptyBody(),
        timeouts: Timeouts = Timeouts.None,
        redirects: Redirects = Redirects.Default,
        forceSend: Boolean = false,
    ): HttpResult2 {
        return engine.request(HttpMethod.Options, url, mergeData(data), body, timeouts, redirects, forceSend)
    }
}
