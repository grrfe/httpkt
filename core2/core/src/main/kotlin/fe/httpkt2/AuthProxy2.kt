package fe.httpkt2

import fe.httpkt.proxy.AuthProxy
import fe.httpkt.proxy.PasswordProxyLogin
import java.net.*

public object AuthProxy2Compat {
    public fun toAuthProxy2(authProxy: AuthProxy): AuthProxy2 {
        val address = authProxy.proxy?.address() ?: return EmptyAuthProxy2
        if (address !is InetSocketAddress) {
            error("Proxy address is not a socket address")
        }

        val wrapper = ProxyWrapper(address.hostName, address.port, authProxy.proxy!!)
        val login = (authProxy.login as? PasswordProxyLogin)?.let {
            PasswordAuthentication(
                it.username,
                it.password.toCharArray()
            )
        }

        return RealAuthProxy2(wrapper, login)
    }
}

public fun AuthProxy2(
    type: Proxy.Type,
    hostname: String,
    port: Int,
    login: PasswordAuthentication? = null,
): AuthProxy2 {
    // internally does a DNS lookup!
    val sa = InetSocketAddress(hostname, port)
    // if lookup fails -> host = hostname, otherwise addr = resolved
    val proxy = Proxy(type, sa)
    val wrapper = ProxyWrapper(hostname, port, proxy)

    return RealAuthProxy2(wrapper, login)
}

public data class ProxyWrapper(
    val hostname: String,
    val port: Int,
    val proxy: Proxy,
)

public interface AuthProxy2 {
    public fun openConnection(url: URL): URLConnection
}

public data object EmptyAuthProxy2 : AuthProxy2 {
    override fun openConnection(url: URL): URLConnection = url.openConnection()
}

public data class RealAuthProxy2(
    val wrapper: ProxyWrapper,
    private val authentication: PasswordAuthentication? = null,
) : AuthProxy2 {
    override fun openConnection(url: URL): URLConnection {
        return url.openConnection(wrapper.proxy)
    }

    public companion object {
        private val defaultAuthenticator = ProxyAuthenticator()

        init {
            Authenticator.setDefault(defaultAuthenticator)
        }
    }

    init {
        if (authentication != null) {
            defaultAuthenticator.addProxy(wrapper, authentication)
        }
    }
}

public class ProxyAuthenticator() : Authenticator() {
    private val proxyLogins: MutableMap<String, PasswordAuthentication> = mutableMapOf()

    private fun makeKey(host: String, port: Int, protocol: String) = "$host:$port:$protocol"

    public fun getProtocol(proxy: Proxy): String = when (Proxy.Type.SOCKS) {
        proxy.type() -> "SOCKS5"
        else -> proxy.type().toString()
    }

    public fun addProxy(wrapper: ProxyWrapper, login: PasswordAuthentication) {
        val protocol = getProtocol(wrapper.proxy)
        proxyLogins.put(makeKey(wrapper.hostname, wrapper.port, protocol), login)
    }

    override fun getPasswordAuthentication(): PasswordAuthentication? {
        val key = makeKey(requestingHost, requestingPort, requestingProtocol)
        val login = proxyLogins[key] ?: return null

        return login
    }
}
