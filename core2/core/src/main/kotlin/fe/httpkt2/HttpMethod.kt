package fe.httpkt2

public sealed interface BaseHttpMethod {
    public val verb: String
}

public enum class HttpNoBodyMethod : BaseHttpMethod {
    Head, Get;

    override val verb: String = name.uppercase()
}

public enum class HttpMethod : BaseHttpMethod {
    Post, Put, Patch, Delete, Options;

    override val verb: String = name.uppercase()
}
