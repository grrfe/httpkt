package fe.httpkt2

public sealed interface Either<out A, out B> {
    @JvmInline
    public value class Left<A>(public val value: A) : Either<A, Nothing>

    @JvmInline
    public value class Right<B>(public val value: B) : Either<Nothing, B>

    public fun <C> map(block: (B) -> C): Either<A, C> = when (this) {
        is Right -> Right(block(value))
        is Left -> this
    }

    public fun <A, C> flatMap(block: (B) -> Either<A, C>): Either<A, C> = when (this) {
        is Right -> block(value)
        is Left -> @Suppress("UNCHECKED_CAST")
        this as Either<A, C>
    }
}

public fun <A> A.left(): Either.Left<A> {
    return Either.Left(this)
}

public fun <B> B.right(): Either.Right<B> {
    return Either.Right(this)
}

public fun <T> Result<T>.asEither(): Either<Throwable, T> {
    return getOrNull()?.let { Either.Right(it) } ?: Either.Left(exceptionOrNull()!!)
}
