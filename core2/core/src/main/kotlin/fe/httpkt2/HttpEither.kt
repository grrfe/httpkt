package fe.httpkt2

public fun HttpResult2.asEither(): Either<Throwable, HttpResponse> {
    return when (this) {
        is HttpFailure -> Either.Left(throwable)
        is HttpResponse -> Either.Right(this)
    }
}

public fun <R, A, B> Either<A, B>.conflate(
    transformLeft: (A) -> R,
    transformRight: (B) -> R,
): R {
    return when (this) {
        is Either.Left<A> -> transformLeft(value)
        is Either.Right<B> -> transformRight(value)
    }
}

public fun <A, B> Either<A, B>.fold(
    foldLeft: (A) -> Either<A, B>,
    foldRight: (B) -> Either<A, B>,
): Either<A, B> {
    return when (this) {
        is Either.Left<A> -> foldLeft(value)
        is Either.Right<B> -> foldRight(value)
    }
}

public fun <R> Either<Throwable, HttpResponse>.mapHttpSuccess(onSuccess: (HttpResponse) -> Either<Throwable, R>): R? {
    val either = when (this) {
        is Either.Left<Throwable> -> null
        is Either.Right<HttpResponse> -> if (value.isSuccess) onSuccess(value) else null
    }

    return when (either) {
        is Either.Right -> either.value
        else -> null
    }
}

public fun <R> Either<Throwable, HttpResponse>.foldHttpSuccess(
    onSuccess: (HttpResponse) -> Either<Throwable, R>,
    onFailure: (HttpResponse) -> Either<Throwable, R>,
): Either<Throwable, R> {
    return when (this) {
        is Either.Left<Throwable> -> this
        is Either.Right<HttpResponse> -> if (value.isSuccess) onSuccess(value) else onFailure(value)
    }
}
