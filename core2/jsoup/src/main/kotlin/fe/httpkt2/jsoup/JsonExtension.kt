package fe.httpkt2.jsoup

import fe.httpkt.html.readToHtmlDocument
import fe.httpkt.html.readToXml
import fe.httpkt.util.StreamType
import fe.httpkt2.Either
import fe.httpkt2.HttpResponse
import fe.httpkt2.asEither
import org.jsoup.nodes.Document
import org.jsoup.parser.Parser
import java.nio.charset.Charset

public  fun HttpResponse.readToHtmlDocumentEither(
    streamType: StreamType = StreamType.Auto,
    charset: Charset = Charsets.UTF_8
): Either<Throwable, Document> {
    return runCatching { connection.readToHtmlDocument(streamType, charset) }.asEither()
}

public fun HttpResponse.readToXmlEither(
    streamType: StreamType = StreamType.Auto,
    charset: Charset = Charsets.UTF_8,
    parser: Parser = Parser.xmlParser()
): Either<Throwable, Document> {
    return runCatching { connection.readToXml(streamType, charset, parser) }.asEither()
}
