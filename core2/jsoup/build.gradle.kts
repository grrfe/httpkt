plugins {
}

dependencies {
    api(project(":core"))
    api(project(":serialization-jsoup"))

    api(project(":core2-core"))
    api("org.jsoup:jsoup:_")

    testImplementation(kotlin("test"))
}
