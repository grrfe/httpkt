plugins {
}


dependencies {
    implementation("com.google.code.gson:gson:_")
    implementation("org.jsoup:jsoup:_")

    implementation(project(":serialization-gson"))
    implementation(project(":serialization-jsoup"))
    implementation(project(":core"))
    implementation(project(":core2-core"))
    testImplementation(kotlin("test"))
}
