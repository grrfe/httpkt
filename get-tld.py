import requests
page = requests.get("https://publicsuffix.org/list/public_suffix_list.dat")

icann_domains = []
for line in page.text.splitlines():
    if "END ICANN DOMAINS" in line:
        break
    elif line.startswith("//"):
        continue
    else:
        domain = line.strip()
        if domain:
            icann_domains.append("." + domain)

print(icann_domains)
with open("core/src/main/resources/tlds.txt", "w") as file:
    file.write("\n".join(icann_domains))
