import fe.buildlogic.extension.asProvider
import fe.buildlogic.extension.getReleaseVersion
import fe.buildlogic.publishing.PublicationComponent
import fe.buildlogic.publishing.publish
import net.nemerosa.versioning.VersioningExtension
import org.gradle.kotlin.dsl.maven
import org.gradle.kotlin.dsl.repositories

plugins {
    kotlin("jvm")
    `maven-publish`
    id("net.nemerosa.versioning")
    id("com.gitlab.grrfe.build-logic-plugin")
}

val baseGroup = "com.gitlab.grrfe.httpkt"

subprojects {
    val isPlatform = name == "platform"
    val isTest = name.endsWith("testing")

    apply(plugin = "net.nemerosa.versioning")
    apply(plugin = "org.gradle.maven-publish")
    apply(plugin = "com.gitlab.grrfe.build-logic-plugin")

    if (!isPlatform) {
        apply(plugin = "org.jetbrains.kotlin.jvm")
    }

    val versionProvider = with(extensions["versioning"] as VersioningExtension) {
        asProvider(this@subprojects)
    }

    group = baseGroup
    version = versionProvider.getReleaseVersion()

    if (!isPlatform) {
        kotlin {
            jvmToolchain(21)
            if (!isTest) {
                explicitApi()
            }
            compilerOptions.freeCompilerArgs.add("-Xwhen-guards")
        }

        java {
            withJavadocJar()
            withSourcesJar()
        }
    }

    if (!isTest) {
        publishing.publish(
            this@subprojects,
            if (isPlatform) PublicationComponent.JavaPlatform else PublicationComponent.Java
        )
    }
}
