# httpkt examples

Table of contents:

- [Introduction](#introduction)
- [A simple first request](#a-simple-first-request)
- [Sending get parameters](#sending-get-parameters)
- [Sending a cookie](#sending-a-cookie)
- [Overriding data](#overriding-data)

## Introduction

The main class used to initiate requests in `httpkt` is [`Request`](../src/main/kotlin/fe/httpkt/Request.kt). An
instance of
`Request` can be populated with headers and cookies which are then sent with every http-request made using this instance.
They can also be overridden on a per-request basis.

## A simple first request

[Runnable example](../src/test/kotlin/fe/example/httpkt/SimpleRequest.kt)

```kotlin
//Create an instance of Request with its default parameters
val request = Request()

//Send a get request to httpbin.org's get endpoint (which returns all data about the request)
val con: HttpURLConnection = request.get("https://httpbin.org/get")

//Print the result of request (readToString() is an extension function, we'll get to that later)
println(con.readToString())
```

This program prints the following:

```json
{
  "args": {},
  "headers": {
    "Accept": "text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2",
    "Cookie": "",
    "Host": "httpbin.org",
    "User-Agent": "Java/14",
    "X-Amzn-Trace-Id": ""
  },
  "origin": "1.2.3.4",
  "url": "https://httpbin.org/get"
}
```

## Sending get parameters

[Runnable example](../src/test/kotlin/fe/example/httpkt/ParameterRequest.kt)

```kotlin
//Create an instance of Request with its default parameters
val request = Request()

//Send a get request to httpbin.org's get endpoint with the get parameters hello=world and foo=bar. Sending the request like
//is equal to sending to sending the url to "https://httpbin.org/get?hello=world&foo=bar"
val con: HttpURLConnection = request.get("https://httpbin.org/get", GetParam("hello", "world"), GetParam("foo", "bar"))

//Print the result of request
println(con.readToString())
```

The response is quite similar to the first example, except that now, the `args` object is populated, which means that we
have successfully sent get parameters.

```json
{
  "args": {
    "foo": "bar",
    "hello": "world"
  }
}
```

## Sending a cookie

[Runnable example](../src/test/kotlin/fe/example/httpkt/CookieRequest.kt)

```kotlin
//Create  an instance of Request with data (a cookie, in this case)
val request = Request(data = {
    //use Cookie.of to build a cookie
    val cookie = Cookie.of("remember", "me")

    //add cookie to this instance
    this.addCookie(cookie)
})

val con = request.get("https://httpbin.org/get")
println(con.readToString())
```

The response tells us that we have successfully sent a cookie to the server:

```json
{
  "args": {},
  "headers": {
    "Accept": "text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2",
    "Cookie": "remember=me",
    "Host": "httpbin.org",
    "User-Agent": "Java/14",
    "X-Amzn-Trace-Id": ""
  },
  "origin": "1.2.3.4",
  "url": "https://httpbin.org/get"
}
```

## Overriding data

[Runnable example](../src/test/kotlin/fe/example/httpkt/DataOverrideRequest.kt)

```kotlin
//Create Request instance with custom header (User-Agent)
val request = Request(data = {
    this.addHeader(Header("User-Agent", "httpkt/13"))
})

//Send get request without overriding header
val conNoOverride = request.get("https://httpbin.org/get")

//User-Agent is "httpkt/13"
println(conNoOverride.readToString())

val conOverride = request.get("https://httpbin.org/get", data = {
    //set custom data for this specific request, override header of Request instance
    this.addHeader(Header("User-Agent", "nothttpkt"))
})

//User-Agent is "nothttpkt"
println(conOverride.readToString())
```

