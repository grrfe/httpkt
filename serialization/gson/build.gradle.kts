plugins {
}

dependencies {
    implementation(project(":core"))
    api("com.google.code.gson:gson:_")

    testImplementation(kotlin("test"))
}
