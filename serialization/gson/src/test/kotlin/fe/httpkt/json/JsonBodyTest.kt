package fe.httpkt.json

import java.io.ByteArrayOutputStream
import kotlin.test.Test
import kotlin.test.assertEquals


internal class JsonBodyTest {
    @Test
    fun test() {
        val expected = """{"hello":"world"}"""

        val map = mapOf("hello" to "world")

        val stream = ByteArrayOutputStream()
        stream.use { JsonBody(toSerialize = map).writeData(it) }
        val result = stream.toByteArray().decodeToString()

        assertEquals(expected, result)
    }
}
