package fe.httpkt.json

import com.google.gson.Gson
import java.util.concurrent.atomic.AtomicReference


public object JsonContext {
    private var instance = AtomicReference<Gson?>()

    public val default: Gson by lazy {
        instance.get() ?: tryLookupGsonContext() ?: Gson()
    }

    public fun init(gson: Gson) {
        if (instance.compareAndSet(null, gson)) return
        error("Context has already been initialized!")
    }

    private fun tryLookupGsonContext(): Gson? {
        try {
            val loader = JsonContext::class.java.classLoader ?: ClassLoader.getSystemClassLoader()
            val clazz = loader.loadClass("fe.gson.context.GlobalGsonContext")
            val instance = clazz.getField("INSTANCE").get(null)

            return clazz.getMethod("getGson").invoke(instance) as Gson
        } catch (e: Exception) {
            return null
        }
    }
}
