package fe.httpkt.json

import com.google.gson.*
import com.google.gson.reflect.TypeToken
import fe.httpkt.ext.getGZIPOrDefaultStream
import fe.httpkt.util.StreamType
import java.lang.reflect.Type
import java.net.HttpURLConnection

/**
 * Read all text from [HttpURLConnection] and parse it using [Gson]
 *
 * @param streamType The stream type to use to read from the [HttpURLConnection], default = [StreamType.Auto]
 * @return the parsed [JsonElement] instance
 * @throws JsonParseException if the body could not be parsed as json
 */
@Throws(JsonParseException::class)
public fun HttpURLConnection.readToJson(
    streamType: StreamType = StreamType.Auto,
): JsonElement = getGZIPOrDefaultStream(streamType).reader().use {
    JsonParser.parseReader(it)
}

@Throws(JsonParseException::class)
public inline fun <reified T : JsonElement> HttpURLConnection.readToJsonElement(
    streamType: StreamType = StreamType.Auto,
) = readToJson(streamType) as T

@Throws(JsonSyntaxException::class, JsonIOException::class)
public inline fun <reified T> HttpURLConnection.fromJson(
    streamType: StreamType = StreamType.Auto,
    gson: Gson = JsonContext.default,
    type: Type = asType<T>(),
): T {
    return getGZIPOrDefaultStream(streamType).reader().use {
        gson.fromJson(it, type)
    }
}

public inline fun <reified T> asType(): Type {
    return object : TypeToken<T>() {}.type
}

public fun HttpURLConnection.readToJsonOrNull(
    streamType: StreamType = StreamType.Auto,
): JsonElement? {
    return runCatching { readToJson(streamType) }.getOrNull()
}

public inline fun <reified T : JsonElement> HttpURLConnection.readToJsonElementOrNull(
    streamType: StreamType = StreamType.Auto,
): T? {
    return readToJsonOrNull(streamType) as? T
}

public inline fun <reified T : JsonElement> HttpURLConnection.readToJsonElementResult(
    streamType: StreamType = StreamType.Auto,
): Result<T> {
    return runCatching { readToJson(streamType) as T }
}

public inline fun <reified T> HttpURLConnection.fromJsonOrNull(
    streamType: StreamType = StreamType.Auto,
    gson: Gson = JsonContext.default,
    type: Type = asType<T>(),
): T? {
    return fromJsonToResult<T>(streamType, gson, type).getOrNull()
}

public inline fun <reified T> HttpURLConnection.fromJsonToResult(
    streamType: StreamType = StreamType.Auto,
    gson: Gson = JsonContext.default,
    type: Type = asType<T>(),
): Result<T> {
    return runCatching { fromJson<T>(streamType, gson, type) }
}
