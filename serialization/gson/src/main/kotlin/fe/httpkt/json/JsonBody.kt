package fe.httpkt.json

import com.google.gson.Gson
import com.google.gson.JsonElement
import fe.httpkt.body.BodyData
import java.io.OutputStream

/**
 * An implementation of [BodyData] allowing the user to send JSON data to the server (application/json)
 * @param json the json payload to send to the server
 */
public class JsonBody(json: String) : BodyData("application/json") {
    private var bytes = json.toByteArray()

    /**
     * @param json a [JsonElement] which should be sent to the server as JSON payload
     */
    public constructor(json: JsonElement) : this(json.toString())

    /**
     * @param toSerialize object to send to the server
     * @param gson [Gson] instance to use for serialization
     */
    public constructor(toSerialize: Any, gson: Gson = JsonContext.default) : this(gson.toJson(toSerialize))

    public companion object {
        public fun fromMap(vararg items: Pair<Any, Any?>, gson: Gson = JsonContext.default): JsonBody {
            return JsonBody(mapOf(*items), gson)
        }

        public fun fromList(vararg items: Any?, gson: Gson = JsonContext.default): JsonBody {
            return JsonBody(items.asList(), gson)
        }
    }

    override fun processContent(): Long {
        return bytes.size.toLong()
    }

    override fun writeData(os: OutputStream) {
        os.write(bytes)
    }
}
