plugins {
}

dependencies {
    implementation(project(":core"))
    api("org.jsoup:jsoup:_")

    testImplementation(kotlin("test"))
}

