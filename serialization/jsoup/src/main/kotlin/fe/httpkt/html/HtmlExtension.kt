package fe.httpkt.html


import fe.httpkt.ext.getGZIPOrDefaultStream
import fe.httpkt.util.StreamType
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.parser.Parser
import java.net.HttpURLConnection
import java.nio.charset.Charset

/**
 * Parse the returned HTML from this [HttpURLConnection]
 *
 * @param streamType The stream type to use to read from the [HttpURLConnection], default = [StreamType.Auto]
 * @return a [Document] representing the HTML
 */
public fun HttpURLConnection.readToHtmlDocument(
    streamType: StreamType = StreamType.Auto,
    charset: Charset = Charsets.UTF_8
): Document = readToXml(streamType, charset, Parser.htmlParser())

public fun HttpURLConnection.readToXml(
    streamType: StreamType = StreamType.Auto,
    charset: Charset = Charsets.UTF_8,
    parser: Parser = Parser.xmlParser()
): Document = getGZIPOrDefaultStream(streamType).use {
    Jsoup.parse(it, charset.toString(), url.toString(), parser)
}
