![](resources/httpkt-banner.png)

`"move fast and break shit" is one of the core principles of this library - so you probably shouldn't use it (also there is
next to no documentation)`

Very simple Python "requests" inspired HTTP-library for easy cookie/header handling written in Kotlin (should also work
with Java).

* Please keep in mind that this library might not suit your purposes at all, since it is VERY basic and might not even
  properly support all HTTP "features". I created it since I didn't want to read the documentation of other libraries
  which seemed very complex for my simple needs.
* Disclaimer: This is a fast-changing library. Upgrading to a new major version (this library follows semver)
  will **DEFINITELY** break your code.

## Features

* Send GET, POST, PATCH, PUT & DELETE requests
* Support for different body data types (FormUrlEncoded, Json, Multipart) (more can be added by extending
  [BodyData](src/main/kotlin/fe/httpkt/body/BodyData.kt))
* Set headers/cookies once and then reuse them for every request
* Use proxies with authentication (only PasswordAuthentication currently supported, but more can be added by extending
  [ProxyLogin](src/main/kotlin/fe/httpkt/proxy/AuthProxy.kt), you are very welcome to submit a PR for that!)
* GZIP detection/support
* Convenience extension functions for `HttpUrlConnection` to read the response, parse it as json or debug headers
* Provides a class-based API which allows you to easily define recurring requests as classes to keep your code clean

**Available [ProxyLogin](src/main/kotlin/fe/httpkt/proxy/AuthProxy.kt) implementations**

* [PasswordProxyLogin](src/main/kotlin/fe/httpkt/proxy/AuthProxy.kt)

**Available [BodyData](src/main/kotlin/fe/httpkt/body/BodyData.kt) implementations**

* [EmptyBody](src/main/kotlin/fe/httpkt/body/EmptyBody.kt): default, don't send any body data
* [FormUrlEncodedBody](src/main/kotlin/fe/httpkt/body/FormUrlEncodedBody.kt): `application/x-www-form-urlencoded`
* [JsonBody](src/main/kotlin/fe/httpkt/body/JsonBody.kt): `application/json`
* [MultipartBody](src/main/kotlin/fe/httpkt/body/MultipartBody.kt): `multipart/form-data`

## Usable via

[![](https://jitpack.io/v/com.gitlab.grrfe/httpkt.svg)](https://jitpack.io/#com.gitlab.grrfe/httpkt)

## Examples

Please refer to [examples](doc/EXAMPLES.md) for examples on how to use this library.

## Branches/Versions

Development is usually done on the `master` branch. Release version numbers follow semver; This means that your code
will, as already mentioned above, most definitely break when you upgrade to a new major version. You should
use the Git tags to fetch this library as a dependency via [jitpack.io](https://jitpack.io).

When fixes and/or minor features need to be backported to older versions, the latest tag of that major version is used
to create a new branch, if it does not already exist.

### Version table

| Branch/Version | Latest                                                       |
|----------------|--------------------------------------------------------------|
| [12.x.x](https://gitlab.com/grrfe/httpkt/-/tree/12.x.x)       | [12.0.1](https://jitpack.io/#com.gitlab.grrfe/request/12.0.1) |
| [11.x.x](https://gitlab.com/grrfe/httpkt/-/tree/11.x.x)       | [11.0.4](https://jitpack.io/#com.gitlab.grrfe/request/11.0.4) |
| [10.x.x](https://gitlab.com/grrfe/httpkt/-/tree/10.x.x)       | [10.0.3](https://jitpack.io/#com.gitlab.grrfe/request/10.0.3) |
| [9.x.x](https://gitlab.com/grrfe/httpkt/-/tree/9.x.x)         | [9.2.3](https://jitpack.io/#com.gitlab.grrfe/request/9.2.3)   |
| [8.x.x](https://gitlab.com/grrfe/httpkt/-/tree/8.x.x)         | [8.0.3](https://jitpack.io/#com.gitlab.grrfe/request/8.0.3)   |
| [7.x.x](https://gitlab.com/grrfe/httpkt/-/tree/7.x.x)         | [7.0.2](https://jitpack.io/#com.gitlab.grrfe/request/7.0.2)   |
| [6.x.x](https://gitlab.com/grrfe/httpkt/-/tree/6.x.x)         | [6.0.3](https://jitpack.io/#com.gitlab.grrfe/request/6.0.3)   |
| [5.x.x](https://gitlab.com/grrfe/httpkt/-/tree/5.x.x)         | [5.0.0](https://jitpack.io/#com.gitlab.grrfe/request/5.0.0)   |
| [4.x.x](https://gitlab.com/grrfe/httpkt/-/tree/rewrite-4.0.0) | [4.3.3](https://jitpack.io/#com.gitlab.grrfe/request/4.3.3)   |
| [3.x.x](https://gitlab.com/grrfe/httpkt/-/tree/3.x.x)         | [3.2.1](https://jitpack.io/#com.gitlab.grrfe/request/3.2.1)   |
| [2.x.x](https://gitlab.com/grrfe/httpkt/-/tree/2.x.x)         | [2.1.1](https://jitpack.io/#com.gitlab.grrfe/request/2.1.1)   |
| [1.x.x](https://gitlab.com/grrfe/httpkt/-/tree/1.x.x)         | [1.0.0](https://jitpack.io/#com.gitlab.grrfe/request/1.0.0)   |
